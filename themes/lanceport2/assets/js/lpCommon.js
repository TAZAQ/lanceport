
// to log
function tlg(data) {
    console.log(data);
}

// делает запрос на сервер,
// my_method: string - название метода
// my_data: object - данные для передачи на сервер
// my_success: function - метод, выполняемый в случае успеха
// my_error: function - метод, выполняемый в случае неудачи
function doReqS(my_method, my_data, my_success, my_error= null) {
    // убирает ложное срабатывание
    if (my_method === 'onGetTaskData' && my_data.task_id === undefined) return;

    $.request(my_method, {
        data: my_data,
        success: my_success,
        error: my_error
    });
}

// удаление класса по шаблону
(function($) {
    $.fn.removeClassWild = function(mask) {
        return this.removeClass(function(index, cls) {
            let re = mask.replace(/\*/g, '\\S+');
            return (cls.match(new RegExp('\\b' + re + '', 'g')) || []).join(' ');
        });
    };
})(jQuery);

// получение данных о проекте
function getProjectData() {
    let project_info = $('#project_info');
    return {
        'project_slug': project_info.attr('data-project-slug'),
        'user_id': project_info.attr('data-user-id')
    }
}