main();

// запуск редактора
function te2CKELoad() {
    if ($('#te2__result_text').length > 0) {
        try {
            CKEDITOR.replace( 'te2__result_text', {'height': '468px'});
        } catch (e) {}
    }
}

function mainReplyLoad() {
    // запуск редактора в обсуждении
    if ($('#lp_new_comment').length > 0) {
        try {
            CKEDITOR.replace( 'lp_new_comment', {'height': '100px'});
        } catch (e) {}
    }
}

function getCommentHashAndRedirect() {
    function getCookie(name) {
        let matches = document.cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));
        return matches ? decodeURIComponent(matches[1]) : undefined;
    }

    comment_hash = getCookie('comment_hash');
    if (comment_hash && (comment_hash !== '')) {
        location.hash = comment_hash;
        document.cookie = `comment_hash=; expires=0;`;
    }
}

function main() {
    te2CKELoad();
    mainReplyLoad();
    getCommentHashAndRedirect();
}