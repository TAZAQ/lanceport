// флаг видимости настроек
let settings_is_hidden = true;


main();

function main() {
    onProjectNewChangeImageInput();
    onCategoryLoadClick();
    onCategoryListLoad();
    onSubtaskAdd();
    onBtnSubtaskSettingsClick();
    onTagBtns();
    onTaskClick();
    onSaveClick();
    onTaskEditorMenuItemClick();
    onTaskDelete();
    onTaskSaveIndexPurge();
    onFilterAndSortsClicks();
    onTasksViewChange();
    onTaskTypeFiltersClick();
    onWorkerTaskFilterClick();
    tableSort();
    replyClick();
}

// вывод изображения при создании проекта
function onProjectNewChangeImageInput() {
    $("#id_project_new").change(function(){
        if (this.files && this.files[0]) {
            let reader = new FileReader();

            reader.onload = function (e) {
                $('#id_project_image').attr('src', e.target.result);
            };

            reader.readAsDataURL(this.files[0]);
        }
    });
}

// метод для заполнения формы изменения категории
function onCategoryLoadClick() {
    $('.category_load').on('click', function () {
        let id = $(this).attr('data-id');
        $('#id_modal_category_load').val(id);

        let m = 'onCategoryLoad';
        let d = {
            'id': id
        };
        let s = function(data) {
            let category = data.data;
            $('#id_category_name_load').val(category.name);
            $('#id_category_description_load').val(category.description);

            onCategoryLoadClick();
        };

        doReqS(m, d, s);
    });
}

// метод для заполнения селектора категорий на #modal_project_new
function onCategoryListLoad() {
    $('.project_item_add').on('click', function () {
        let id = +$(this).attr('data-id');

        let m = 'onCategoryListLoad';
        let d = {};
        let s = function(data) {
            $('#id_modal_project_new_category').text('');

            let categories = data.data;

            categories.forEach(function (category, i) {
                let is_selected = (category.id === id) ? 'selected' : '';
                let option = `<option value="${category.id}" ${is_selected}>${category.name}</option>`;

                $('#id_modal_project_new_category').append(option);
            });
        };

        doReqS(m, d, s);
    });
}

// метод для добавления подзадачи
function onSubtaskAdd() {
    $('#id_btn_subtask_add').on('click', function () {
        createSubtask();
    });
}

// метод создания подзадачи
function createSubtask(subtask_text="", subtask_id="", is_checked="") {
    let subtask_container = $('#te2__subtask_container');
    let subtask_count = subtask_container[0].childElementCount + 1;
    let classHidden = "hidden";
    if (settings_is_hidden === false) classHidden = "";

    subtask_text = subtask_text === "" ? `Новая подзадача ${subtask_count}` : subtask_text;

    let subtask = `
            <div class="uk-flex uk-flex-middle subtask">
                <input type="hidden" name="subtasks[${subtask_count}][is_removed]" value="">
                <input type="hidden" name="subtasks[${subtask_count}][id]" value="${subtask_id}">
                <input type="checkbox" class="uk-checkbox lp-offcanvas-input subtask-checkbox" name="subtasks[${subtask_count}][is_done]" ${is_checked}>
                <input type="text" class="uk-input lp-offcanvas-input subtask-input" name="subtasks[${subtask_count}][name]" value="${subtask_text}">

                <div class="uk-flex subtasks_settings ${classHidden}">
                    <div class="lp-btn btn_subtask_del"><i uk-icon="icon: trash; ratio: 1.5"></i></div>
                    <div class="lp-btn"><i uk-icon="icon: menu; ratio: 1.5"></i></div>
                </div>
            </div>
        `;

    subtask_container.append(subtask);

    delSubtaskListener();
    teProgressRecount();

    // пересчёт прогресса после нажатия на чекбокс
    $('.subtask-checkbox').on('click', function () {
        teProgressRecount();
    });
}

// удаление подзадачи
function delSubtaskListener() {
    $('.btn_subtask_del').on("click", function() {
        let this_parent = $(this).parents('.subtask');
        this_parent.children('input')[0].value = 1;
        this_parent.attr('hidden', 'hidden');
        teProgressRecount();
    });
}

// реализация клика на настройки подзадачи
function onBtnSubtaskSettingsClick() {
    $('#id_btn_show_settings').on('click', function () {
        if (settings_is_hidden === true) {
            $('.subtasks_settings').removeClass('hidden');
        } else {
            $('.subtasks_settings').addClass('hidden');
        }

        settings_is_hidden = !settings_is_hidden;
        delSubtaskListener();
    });
}

// пересчитывание прогресса
function teProgressRecount() {
    let subtasks = $('#te2__subtask_container .subtask[hidden!="hidden"] .subtask-checkbox');
    let subtasks_count = subtasks.length;

    let is_done = 0;
    if (subtasks_count) {
        subtasks.each(function (i, item) {
            is_done += item.checked;
        });

        $('#te2__progress').val(100 * is_done / subtasks_count);
    } else {
        $('#te2__progress').val(is_done);
    }
}

// кнопки разделов тегов
function onTagBtns() {
    // добавить тег
    $('#te2__tag_add').on('click', function () {
        let tag_text = $('#te2__tag_input').val();
        createTag(tag_text);
    });

    // очистить поле
    $('#te2__tag_clr').on('click', function () {
        $('#te2__tag_input').val("");
    });
}

// метод создания тега + вычисление его размера
function createTag(tag_text, tag_id='') {
    if (tag_text.length >= 1) {
        let tag_text_width = tag_text.length * 8;

        let tag_template = `
                <span class="task-tag uk-badge lp-badge">
                    <input type="hidden" name="tags[][is_removed]" value>
                    <input type="hidden" name="tags[][id]" value="${tag_id}">
                    <input type="text" name="tags[][name]" value="${tag_text}" readonly style="width: ${tag_text_width}px">
                    <i class="uk-text-secondary te2__tag_rmv" uk-icon="close"></i>
                </span>
            `;


        $('#te2__tag_container').append(tag_template);
        tagRemove();
    }
}

// удаление тега
function tagRemove() {
    // удалить тег
    $('.te2__tag_rmv').on('click', function () {
        let this_parent = $(this).parents('.task-tag');
        this_parent.children('input')[0].value = 1;
        this_parent.attr('hidden', 'hidden');
    });
}

// нажатие на задачу
function onTaskClick() {
    $('.task').on('click', function () {
        let task_id = this.dataset.id;
        let task = document.getElementById('te2_task_id');
        task.dataset.id = task_id;
        $('#lp-task-editor-v2--body input').val('');
        UIkit.tab($('.uk-tab-left')).show(0);
        onLoadTaskData(0, task_id);
    });
}

// метод записывающий данные о проекте в поля
function onSaveClick() {
    $('.te2_save').on('click', function () {
        let task = document.getElementById('te2_task_id');
        let task_id = task.dataset.id;
        let prj = getProjectData();

        $('.te2-task').val(task_id);
        $('.te2-slug').val(prj.project_slug);
        $('.te2-user').val(prj.user_id);
    });
}

// нажатие на пункт меню в редакторе
function onTaskEditorMenuItemClick() {
    $('.te2_menu_item').on('click', function () {
        let item_index = this.dataset.item;
        let task = document.getElementById('te2_task_id');
        onLoadTaskData(item_index, task.dataset.id);
    });
}

function onLoadTaskData(item_index= 0, task_id = 0) {
    if (!task_id) return false;

    // method name
    const method = [
        'onGetTaskMainData',
        'onGetTaskStatusData',
        'onGetTaskReportData',
        'onGetTaskSubtasksData',
        'onGetTaskTagsData',
        'onGetTaskParentData',
        'onGetTaskStoryData',
    ];
    // данные о проекте
    let project_info = getProjectData();
    $('.te2_button_comments').attr('href', `/projects/${project_info.project_slug}/tasks/${task_id}`);

    let m = 'onGetTaskData_TE2';
    let d = {
        'project_slug': project_info.project_slug,
        'user_id': project_info.user_id,
        'task_id': task_id,
        'method_index': item_index
    }
    let s = function(data) {
        onShowLoadedTaskData(data, item_index);
    }

    doReqS(m, d, s);
}

// метод отправки данных удаляемой задачи
function onTaskDelete() {
    $('#te__btn_del').on('click', function () {
        let prj_info = $('#id_prj_info');

        let m = 'onTaskDelete';

        let d = {
            'project_id': prj_info.attr('data-project'),
            'user': prj_info.attr('data-user'),
            'task_id': $('#te__task_id').val()
        };

        let s = function(data) {
            onTaskDeleteShow(data);
            initCounters();
        };

        doReqS(m, d, s);
    });
}

// пересчёт индексов у инпутов для правильного упорядочивания
function onTaskSaveIndexPurge() {
    $('#te__btn_save').on('click', function () {
        // количество инпутов подзадач за раз
        const subtask_input_group = 4;

        let subtask_container = $('#te__subtask_container');
        let subtask_inputs = subtask_container.find('.subtask input');

        let subtask_group_count = +(subtask_inputs.length / subtask_input_group);
        for (let i = 0; i < subtask_group_count; i++) {
            let iter = subtask_input_group * (i + 1);
            subtask_inputs[iter - 4].setAttribute('name', `subtasks[${i}][is_removed]`);
            subtask_inputs[iter - 3].setAttribute('name', `subtasks[${i}][id]`);
            subtask_inputs[iter - 2].setAttribute('name', `subtasks[${i}][is_done]`);
            subtask_inputs[iter - 1].setAttribute('name', `subtasks[${i}][name]`);
        }

        // количество инпутов тегов за раз
        const tag_input_group = 3;
        let tag_container = $('#te__tag_container');
        let tags_inputs = tag_container.find('.task-tag input');
        let tag_group_count = +(tags_inputs.length / tag_input_group);
        for (let i = 0; i< tag_group_count; i++) {
            let iter = tag_input_group * (i + 1);
            tags_inputs[iter - 3].setAttribute('name', `tags[${i}][is_removed]`);
            tags_inputs[iter - 2].setAttribute('name', `tags[${i}][id]`);
            tags_inputs[iter - 1].setAttribute('name', `tags[${i}][name]`);
        }
    });
}


// реализация фильтров
function onFilterAndSortsClicks() {
    try {
        // сортировка по умолчанию
        const sort_default = 'sort: id; order: desc';

        // глобальные
        let method_filter_string = "";
        let method_sorting_string = sort_default;

        // общий метод для постановки атрибута
        function setFilterAndSort() {
            // uikit класс фильтра
            const filter_attribute = "uk-filter-control";

            // итоговый фильтр
            let full_filter = "";
            if (method_filter_string === '') {
                full_filter = method_sorting_string;
            } else {
                full_filter = method_filter_string + ';' + method_sorting_string;
            }

            // установка фильтра
            lp_filters_settings.setAttribute(filter_attribute, full_filter);

            // костыль =)
            $('#lp_filters_settings').trigger('click');
        }

        // установить сортировку
        function setSort(value=sort_default) {
            method_sorting_string = value;
        }

        // сбросить сортировку
        function cancelSort() {
            setSort();
        }

        // для сортировок***********************************************************************************************
        $('.lp-sorts-radio').click(function () {
            const sorts_obj = {
                'data-priority': 'sort: data-priority; order: desc',
                'data-date':     'sort: data-date; order: desc',
                'data-score':    'sort: data-score; order: desc',
            };

            // получение типа сортировки
            let sort_type = this.id;
            switch (sort_type) {
                case 'sort_type_1':
                    setSort(sorts_obj["data-priority"]);
                    break;
                case 'sort_type_2':
                    setSort(sorts_obj["data-date"]);
                    break;
                case 'sort_type_3':
                    setSort(sorts_obj["data-score"]);
                    break;
                default:
                    cancelSort();
                    break;
            }

            setFilterAndSort();
        });
    }
    catch (e) {}
}

// реализация минимизации задач
function onTasksViewChange() {
    try {
        $('input[name=tasks_view_type]').change(function () {
            if (this.id === 'tasks_view_0') {
                $('.task-items').show();
            } else {
                $('.task-items').hide();
            }
        });
    }
    catch (e) {}
}

// фильтры по типу задач
function onTaskTypeFiltersClick() {
    $('#lp_task_types_filters, #lp_filters').on('click', function () {
        makeFilter();
    });
}

// двойной фильтр по задачам
function makeFilter() {
    // все итемы
    let filter_priorities = $('#lp_filters').find('.lp-filters-wrapper .lp-filter-cbox');
    let filter_types = $('#lp_task_types_filters').find('.lp-filters-wrapper .lp-filter-cbox');

    // формирование матрицы фильтрации
    let filter_matrix = [];
    for (let p = 0; p < filter_priorities.length; p++) {
        filter_matrix[p] = [];
        for (let t = 0; t < filter_types.length; t++) {
            filter_matrix[p][t] = filter_priorities[p].checked && filter_types[t].checked;
        }
    }

    $('.task').hide();

    // отображение
    const cb_worker = document.getElementById('cb_worker');
    let worker = cb_worker.dataset.worker;
    let worker_class = `.task[data-worker='${worker}']`;
    for (let p = 0; p < filter_priorities.length; p++) {
        for (let t = 0; t < filter_types.length; t++) {
            if (filter_matrix[p][t]) {
                let css_priority = $(filter_priorities[p]).attr('data-css');
                let css_type = $(filter_types[t]).attr('data-css');
                if ( !$(cb_worker).is(':checked') ) worker_class = '';
                $(worker_class+css_priority+css_type).show();
            }
        }
    }
}

// фильтр по задачам работника
function onWorkerTaskFilterClick() {
    const cb_worker = $('#cb_worker');
    cb_worker.on('change', function() {
        function doFilter() {
            if ( cb_worker.is(':checked') ) {
                $(`.task[data-worker!=${worker_id}]`).hide();
            } else {
                $(`.task[data-worker!=${worker_id}]`).show();
            }
        }

        let worker_id = cb_worker.attr('data-worker');
        if ( !worker_id ) {
            let user_id = $('#id_prj_info').attr('data-user')
            let m = `onGetWorkerId`;
            let d = {
                'real_user_id': user_id
            }
            s = function(data) {
                worker_id = data.id
                cb_worker.attr('data-worker', worker_id);
                doFilter();
            }

            doReqS(m, d, s);
        } else {
            doFilter();
        }
    });
}

// сортировка таблицы трекера
function tableSort() {
    const table = document.getElementById('tracker_table');
    const table_header_css = '.tracker_table_th';
    const asc_sort_class = 'uk-text-primary';
    const desc_sort_class = 'uk-text-danger';

    $(table_header_css).on('click', function () {
        $(table_header_css).removeClass(asc_sort_class);
        $(table_header_css).removeClass(desc_sort_class);

        let col = $(this).attr('data-col');
        let sort_type = +$(this).attr('data-sort-type');

        let sortedRows = Array.from(table.rows)
            .slice(1)
            .sort((rowA, rowB) =>
                rowA.cells[col].dataset.sort > rowB.cells[col].dataset.sort ? (-1) ** sort_type : -1 * (-1)**(sort_type) );

        table.tBodies[0].append(...sortedRows);
        sort_type = sort_type === 0 ? 1 : 0;
        $(this).attr('data-sort-type', sort_type);

        if (sort_type === 0) {
            $(this).addClass(asc_sort_class);
        } else {
            $(this).addClass(desc_sort_class);
        }
    });
}

function replyClick() {
    $('.lp_comment_reply').on('click', function () {
        let comment_id = $(this).attr('data-id');
        $('#comment_reply_id').val(comment_id);

        $('#reply_form').appendTo('#comment_reply_editor_container__'+comment_id).show();
        try {
            if (CKEDITOR.instances.lp_reply_new_comment) CKEDITOR.instances.lp_reply_new_comment.destroy();
            CKEDITOR.replace( 'lp_reply_new_comment', {'height': '100px'});
            CKEDITOR.instances.lp_reply_new_comment.setData( '', function() {
                CKEDITOR.instances.lp_reply_new_comment.resetDirty();
            });
        } catch (e) {}
    });
}
