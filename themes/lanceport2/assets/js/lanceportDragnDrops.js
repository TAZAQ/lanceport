const id_counter_text = '_counter';
const id_state_text = 'state_';

main();

function main() {
    dragTaskBacklogToSprints();
    dragTaskToState();
}

// удаление отступа у задачи после дропа
// смена спринта
function dragTaskBacklogToSprints() {
    UIkit.util.on('.tasks-container', 'added', function (element, options) {
        let task_data = $($(element.detail[1])[0]);
        let container_data = $(element.target)[0];

        // добавление/удаление отступа
        if ($(container_data).attr('data-container-name') === 'sprint') {
            $(container_data).children('.task').removeClass('uk-margin-small-bottom');
        } else {
            $(container_data).children('.task').addClass('uk-margin-small-bottom');
        }

        let prj_info = $('#id_prj_info');
        let task_id = $(task_data).attr('data-id');
        let sprint_id = $(container_data).attr('data-id');
        sprint_id = sprint_id ? sprint_id : -1;

        let m = 'onSprintTaskMoved';
        let d = {
            'project_id': prj_info.attr('data-project'),
            'user_id': prj_info.attr('data-user'),
            'task_id': task_id,
            'sprint_id': sprint_id
        };

        let s = null;

        doReqS(m, d, s);
    });
}

// инициализация счётчиков
function initCounters(counter_num=5) {
    try {
        for (let counter_item = 1; counter_item <= counter_num; counter_item++) {
            let state_id = '#' + id_state_text + counter_item;
            let counter_id = state_id + id_counter_text;
            $(counter_id).text( $(state_id)[0].childElementCount );
        }
    } catch (e) {tlg('Страница проекта')}
}

// обновление счётчиков
// смена состояния задачи
function dragTaskToState() {
    try {
        // инициализация счётчиков
        initCounters();

        // при добавлении
        UIkit.util.on('.task-container', 'added', function (item) {
            onMoved(item);
        });

        // при перемещении в пределах одного состояния
        UIkit.util.on('.task-container', 'stop', function (item) {
            // onMoved(item);
        });

        // само перемещение
        function onMoved(item) {
            const m = 'onTaskStateChange';

            let to_state_id = '#' + item['target'].id;
            let to_state_counter_id = to_state_id + id_counter_text;

            let task_id = $(item)[0].detail[1].getAttribute('data-id');
            let new_state = item['target'].getAttribute('data-state-id');
            let pos_in_state = $('#' + $(item)[0].detail[1].id).index();

            let prj_info = $('#id_prj_info');
            let d = {
                'project_id': prj_info.attr('data-project'),
                'user_id': prj_info.attr('data-user'),
                'task_id': task_id,
                'new_state': new_state,
                'pos_in_state': pos_in_state
            };

            let s = function(data) {
                $(to_state_counter_id).text(
                    $(to_state_id)[0].childElementCount
                );
            };

            // отправка запроса
            doReqS(m, d, s);
        }

        // при удалении
        UIkit.util.on('.task-container', 'removed', function (item) {
            let from_state_id = '#' + item['target'].id;
            let from_state_counter_id = from_state_id + id_counter_text;

            $(from_state_counter_id).text(
                $(from_state_id)[0].childElementCount
            );
        });
    }
    catch (e) {}
}
