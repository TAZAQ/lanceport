// добавление новой категории на экран
function onCategoryNewShow(data) {
    let category = data.data;

    let template = `
        <li id="id_category_${category.id}">
            <a class="uk-accordion-title" href="/categories/${category.slug}" title="${category.description}">
                ${category.name} [0 пр.] <i uk-icon="pencil"></i>
            </a>
            <div class="uk-accordion-content project-list-container">
            </div>
        </li><!-- категория -->
    `;

    $('#id_category_container').append(template);
}


// обновление названия, описания и ссылки на экране
function onCategoryUpdate(data) {
    let category_id = '#id_category_url' + data.id;
    $(category_id).attr('href', '/categories/' + data.slug);
    $(category_id).attr('title', data.description);
    $(category_id).text(data.name);
}

// добавление нового проекта на экран
function onProjectNewShow(data) {
    let category_id = '#id_category_id' + data.category_id;
    let template = `
        <a class="project-item uk-card uk-card-default" href="./projects/${data.slug}">
            <div class="project-item-image"></div>
            <div class="project-item-name">
                <span>${data.name}</span>
            </div>
        </a>
    `;

    $(category_id).children('.project-list-container').prepend(template);

    // обновление счётчика
    let children_count = $(category_id).children('.project-list-container').children('.project-item').length - 1;
    let id = '#id_category_url' + data.category_id;
    let category_text = $(id).text();
    let new_category_text = category_text.slice(0, category_text.indexOf(' [')) + ` [${children_count} пр.]`;

    $(id).text(new_category_text);
}

function onProjectDataShow(data) {
    data = data[1];
    if ( !data ) return;

    let img_path = data.img_path ? data.img_path : '/themes/lanceport2/assets/images/project_thumb.png';
    $('.project-item-image').find('img').attr('img', img_path);
    $('.project-item-name').text(data.name);
    $('.header-project-title').text(data.name);
    $('.project-item-descr').text(data.descr)
}

// метод отображающий новую задачу в проекте
function onProjectTaskShow(data) {
    let task_id = data.id;
    let name = data.name;
    let css = data.css;

    let template = `
        <div class="task uk-card uk-card-default uk-card-body uk-margin-small-bottom ${css}" 
        uk-toggle="target: #task_editor_v2"
        id="id_task_${task_id}" data-id="${task_id}">
            ${name}
        </div>
    `;

    $('#id_backlog_container').prepend(template);
    onTaskClick();
    onTaskDelete();
    initCounters();
}

// метод отображающий новый спринт в проекте
function onProjectSprintShow(data) {
    if (! data) return null;

    let template = `
        <li class="uk-open">
            <a class="uk-accordion-title uk-flex" href="projects/${data['project_slug']}/sprints/${data['id']}">
                <span class="sprint-name">${data['name']}</span>
                <span class="sprint-period">
                    [${data['date_start']} - ${data['date_finish']}]
                </span>
            </a>
            <div class="uk-accordion-content tasks-container"
                 uk-sortable="group: tasks-container; cls-empty: task-in-sprint-placeholder; animation: 0"
                 cls-placeholder="task-cls-placeholder"
                 data-container-name="sprint"
                 data-id="${data['id']}"
            >
            </div>
        </li>
    `;

    $('#id_sprints_container').prepend(template);
}

// обновление спринта
function onSprintDataShow(data) {
    if ( !(data = data[1]) ) return;
    $('.header-sprint-title').text(data.name);
}

// метод удаляющий задачу с экрана
function onTaskDeleteShow(data) {
    if (! data) return null;

    if (data.id) {
        $('#id_task_' + data.id).remove();
    }
}

// метод обновляющий внешний вид задачи
function onTaskUpdateView(data) {
    let task = $('#id_task_' + data.id);

    // удаление классов
    task.removeClassWild('task-*');

    //новые классы
    task.addClass(data.task_type).addClass(data.task_priority);

    // смена заголовка
    let sprint_task = task.find('.task-title');
    if (sprint_task) {
        sprint_task.text(data.name);
    } else {
        task.text(data.name);
    }

    // смена estimate
    let sprint_task_estimate = task.find('.task-scores');
    if (sprint_task_estimate) {
        sprint_task_estimate.text(data.estimate);
    }

    // обновление счётчиков подзадач
    let sprint_task_subtasks = task.find('.task-counter');
    if (sprint_task_subtasks) {
        sprint_task_subtasks.text(`${data.sub}/${data.sub_done}`);
    }

    // смена спринта
    let sprint_id = data.sprints ? +data.sprints.id : -1;
    let this_sprint_id = +$('#id_prj_info').attr('data-sprint');
    if (sprint_task.length !== 0) {
        if ( this_sprint_id !== sprint_id ) {
            task.remove();
        }
    } else {
        let current_task_container_name = task.parents('.tasks-container').attr('data-container-name');
        let current_task_container_id = task.parents('.tasks-container').attr('data-id');
        // переход между спринтами/бэклогом
        if (sprint_id === -1 && current_task_container_name !== 'backlog') {
            $(task).appendTo('#id_backlog_container');
        } else if (sprint_id !== -1 && sprint_id !== current_task_container_id){
            $(task).appendTo('#id_sprint_id'+sprint_id);
        }
    }


    // смена состояния
    let state_id = +data.task_states.id;
    if (!isNaN(this_sprint_id)) {
        $(task).appendTo('#state_'+state_id);
    } else {
        if (state_id === 5) {
            $(task).addClass('task_done');
        } else {
            $(task).removeClass('task_done');
        }
    }
}


// метод выводящий задачу спринта на экран
function onSprintTaskShow(data) {
    let task_time = Date.parse(data.created_at.date)/1000;
    let template = `
        <div class="task uk-card uk-card-default uk-card-body uk-margin-small-bottom ${data.css}"
             id="id_task_${data.id}"
             uk-toggle="target: #task_editor_v2"
             data-id="${data.id}"
             data-orig-pos="${data.name}"
             data-worker="${data.worker}"
             data-score="${data.worker}"
             data-priority="${data.priority}"
             data-task-type="${data.type}"
             data-date="${task_time}"
        >
    
            <div class="uk-text-small task-title">
                ${data.name}
            </div>
            <div class="task-items uk-text-right non-select">
                <span class="uk-text-meta lp-text-smaller uk-float-left">Только что</span>
                <span class="uk-text-meta uk-badge lp-text-smaller task-scores">1.00</span>
                <span class="task-counter uk-float-right">0/0</span>
                <span class="icon-fix uk-float-right" uk-icon="list"></span>
            </div>
        </div>
    `;

    $('#state_1').prepend(template);
    onTaskClick();
    onTaskDelete();
    initCounters();
}

function onErrorShow() {
    // TODO: реализовать всплывающее сообщение
}

function onShowLoadedTaskData(data, item_index) {
    if (data.error) {
        onErrorShow(data.error);
        return null;
    }

    const method = [
        onShowTaskMainData,
        onShowTaskStatusData,
        onShowTaskReportData,
        onShowTaskSubtasksData,
        onShowTaskTagsData,
        onShowTaskParentData,
        onShowTaskStoryData,
    ];

    method[item_index](data);
}

function onShowTaskMainData(data) {
    $('#te2__name').val(data.name);
    $('#te2__estimate').val(data.estimate);
    $('#te2__descr').val(data.descr);
    $('#te2__extra').val(data.extra);
    $('#te2__is_closed').prop('checked', data.is_closed);
}

function onShowTaskStatusData(data) {
    // исполнитель
    let task_lpuser = $('#te2__worker');
    task_lpuser.find('option').remove();
    task_lpuser.append(`<option value="-1" selected>---Без исполнителя---</option>`);

    data.all_project_users.forEach(function (item, i) {
        let selected_option;
        if (data.worker) {selected_option = (data.worker === item.id) ? 'selected' : '';}
        task_lpuser.append(`<option value="${item.id}" ${selected_option}>${item.name}</option>`);
    });

    // состояние задач
    let task_state_selector = $('#te2__task_state');
    task_state_selector.find('option').remove();
    data.all_task_states.forEach(function (item, i) {
        let selected_option = (data.task_state === item.id) ? 'selected' : '';
        task_state_selector.append(`<option value="${item.id}" ${selected_option}>${item.name}</option>`);
    });

    // типы задач
    let task_types_selector = $('#te2__task_type');
    task_types_selector.find('option').remove();
    data.all_task_types.forEach(function (item, i) {
        let selected_option = (data.task_type === item.id) ? 'selected' : '';
        task_types_selector.append(`<option value="${item.id}" ${selected_option}>${item.name}</option>`);
    });

    // приоритет
    let task_priority = $('#te2__priority');
    task_priority.find('option').remove();
    data.all_priorities.forEach(function (item, i) {
        let selected_option = (data.task_priority === item.id) ? 'selected' : '';
        task_priority.append(`<option value="${item.id}" ${selected_option}>${item.name}</option>`);
    });

    // спринт
    let task_sprint = $('#te2__sprint');
    task_sprint.find('option').remove();
    task_sprint.append(`<option value="-1" selected>---Без спринта---</option>`);
    data.all_sprints.forEach(function (item, i) {
        let selected_option = '';
        if (data.sprint) {selected_option = (data.sprint.id === item.id) ? 'selected' : '';}
        task_sprint.append(`<option value="${item.id}" ${selected_option}>${item.name}</option>`);
    });
}

function onShowTaskReportData(data) {
    $('#te2__in_report').prop('checked', data.in_report);
    CKEDITOR.instances.te2__result_text.setData( data.result_text, function() {
        CKEDITOR.instances.te2__result_text.resetDirty();
    });
}

function onShowTaskSubtasksData(data) {
    /* Подзадачи */
    let subtasks = $('#te2__subtask_container');
    subtasks.find('.subtask').remove();
    data.forEach(function (item, i) {
        // если актуальное(в списке это task_states[0]) состояние подзадачи "done", то добавляем checked
        let is_checked = item.task_state_id === 5 ? 'checked' : '';
        createSubtask(item.name, item.id, is_checked);
    });

}

function onShowTaskTagsData(data) {
    /* Теги */
    let task_tags = $('#te2__tag_container');
    task_tags.find('.task-tag').remove();
    data.forEach(function (item, i) {
        createTag(item.name, item.id);
    });
}

function onShowTaskParentData(data) {
    // задача-родитель
    let task_parents = $('#te2__parent');
    task_parents.find('option').remove();
    task_parents.append(`<option value>---Корень---</option>`);
    data.all_project_tasks.forEach(function (item, i) {
        let selected_option;
        if (data.parent) {
            selected_option = (data.parent.id === item.id) ? 'selected' : '';
        }
        task_parents.append(`<option value="${item.id}" ${selected_option}>${item.name}</option>`);
    });

    makeSigmaGraph(data.tree);
}

function makeSigmaGraph(tree) {
    let nodes = [];
    let edges = [];
    let task = document.getElementById('te2_task_id');
    let task_id = task.dataset.id;
    // t - tree
    // p - parrent
    // d - depth
    let y = 0;
    function dfs(t, p, d) {
        let _x = 50 * d;

        $.each(t, function (i, item) {
            y += 7;
            let _color;
            switch (item.task_type_id) {
                default: _color = '#000'; break;
                case 1: _color = '#3b832d'; break;
                case 3: _color = '#bea929'; break;
                case 4: _color = '#ba2b2b'; break;
            }
            _color = item.id == task_id ? '#003dff' : _color ;

            nodes.push({
                'id': 'n' + item.id,
                'label': item.name,
                'x': _x,
                'y': y,
                'size': 2,
                'color': _color,
                'item_id': item.id
            });

            // если есть родитель
            if (p) {
                edges.push({
                    'id': 'e' + edges.length,
                    'source': 'n' + p,
                    'target': 'n' + item.id,
                    'size': 2,
                    'color': '#ccc',
                    'type': 'curve',
                });
            }

            dfs(item.children, item.id, d + 1);

            y += 7;
        });
    }

    // отрисовка дерева
    dfs(tree, null,0, 0);

    $('#te2__sigma').text('');
    let s = new sigma({
        graph: {
            'nodes': nodes,
            'edges': edges
        },
        container: 'te2__sigma',
        settings: {
            edgeLabelSize: 'proportional',
        }
    });

    s.bind('clickNode', function (e) {
        let id = e.data.node.item_id;
        $(`#te2__parent [value="${id}"]`).attr("selected", "selected");
    });

    s.refresh();
}

function onShowTaskStoryData(data) {
    let pagination_item_class = 'te2_pagination_item';
    let content = $('#te2__story_container_content');
    let paginator = $('#te2__story_container_paginator');
    content.text('');
    paginator.text('');

    // TODO: настроить стили + разделить контейнер на записи и пагинатор

    data.pages.data.forEach(function (item) {
        let record = JSON.parse(item.data);
        let keys = Object.keys(record);

        let story_content_header = item.created_at;
        let story_content_body = '';
        for (const key of keys) {
            story_content_body += `<div class="uk-h4 uk-margin-remove-bottom">${key}</div>`;
            record[key].forEach(function (record_item) {
                story_content_body += `
                    <div>
                        <span class="uk-text-primary">${record_item.label}</span>:  ${record_item.value}
                    </div>
                `;
            });
        }
        let template = `
            <div class="lp-story-container lp-story-right">
                <div class="lp-story-content-avatar-container">
                    <a href="/users/${item.lpuser_id}"><img src="${item.lpuser.avatar.path}" alt=""></a>
                </div>
                <div class="lp-story-content">
                    <div class="uk-h3 uk-margin-remove-vertical">${story_content_header}</div>
                    ${story_content_body}
                </div>
            </div>
        `;

        content.append(template);
    });

    paginator.append(getPaginationTemplate(data.pages, pagination_item_class));

    $('.'+pagination_item_class).on('click', function () {
        // данные о проекте
        let project_info = getProjectData();

        let m = 'onGetTaskData_TE2';
        let d = {
            'project_slug': project_info.project_slug,
            'user_id': project_info.user_id,
            'task_id': data.task_id,
            'method_index': 6,
            'page': this.dataset.page
        }
        let s = onShowTaskStoryData;
        doReqS(m, d, s);
    });
}

function getPaginationTemplate(data, css_class) {
    function getPageFromUrl(page_url) {
        if (!page_url) return 1;
        return +page_url.slice(page_url.indexOf('=')+1);
    }

    let cur = data.current_page;
    let first = getPageFromUrl(data.first_page_url);
    let last = data.last_page;
    let prev = getPageFromUrl(data.prev_page_url);
    let next = getPageFromUrl(data.next_page_url);

    let cur_template = `<li class="uk-active"><span>${cur}<span></li>`;
    let first_template = `<li><a href="#" class="te2_pagination_item" data-page="${first}">${first}</a></li>`;
    let last_template = `<li><a href="#" class="te2_pagination_item" data-page="${last}">${last}</a></li>`;
    let prev_template = `<li><a href="#" class="te2_pagination_item" data-page="${prev}">${prev}</a></li>`;
    let next_template = `<li><a href="#" class="te2_pagination_item" data-page="${next}">${next}</a></li>`;
    let placeholder1 = placeholder2 = '<li class="uk-disabled"><span>...</span></li>';

    if (cur == 1 || cur == 2) {
        prev_template = placeholder1 = '';
        if (cur == 1) first_template = '';

        switch (last) {
            case cur: prev_template = next_template = placeholder2 = last_template = ''; break;
            case cur+1: prev_template = next_template = placeholder2 = ''; break;
            case cur+2: prev_template = placeholder2 = ''; break;
        }
    } else if (cur == 3) {
        placeholder1 = '';
        switch (last) {
            case 3: next_template = last_template = placeholder2 = ''; break;
            case 4: last_template = placeholder2 = ''; break;
            case 5: placeholder2 = ''
        }
    } else if (cur == last - 2) {
        placeholder2 = '';
    }
    else if (cur == last - 1 || cur == last) {
        next_template = placeholder2 = '';
        if (cur == last) last_template = '';
    }

    return `
        <ul class="uk-pagination uk-flex-center" uk-margin>
            ${first_template}
            ${placeholder1}
            ${prev_template}
            ${cur_template}
            ${next_template}
            ${placeholder2}
            ${last_template}
        </ul>
    `;
}

function onTaskMainUpdateView(data) {
    data = data[1]; // 0 элемент - сообщение от сервера
    if (!data) return;
    let task = $('#id_task_' + data.id);
    // смена заголовка
    let sprint_task = task.find('.task-title');
    if (sprint_task.length > 0) { sprint_task.text(data.name); } else { task.children('.task-name').text(data.name); }

    // смена estimate (спринт)
    let sprint_task_estimate = task.find('.task-scores');
    if (sprint_task_estimate.length > 0) { sprint_task_estimate.text(data.estimate); }
}

function onTaskStatusUpdateView(data) {
    data = data[1]; // 0 элемент - сообщение от сервера
    if (!data) return;
    let task = $('#id_task_' + data.id);

    // удаление классов
    task.removeClassWild('task-*');
    // приоритет + тип задачи
    task.addClass(data.task_type.css).addClass(data.task_priority.css);

    // смена спринта
    let sprint_id = data.sprint ? +data.sprint.id : -1;
    let this_sprint_id = +$('#id_prj_info').attr('data-sprint');
    let sprint_task = task.find('.task-title');
    if (sprint_task.length !== 0) {
        if ( this_sprint_id !== sprint_id ) { $(task).hide(); }
    } else {
        let current_task_container_name = task.parents('.tasks-container').attr('data-container-name');
        let current_task_container_id = task.parents('.tasks-container').attr('data-id');
        // переход между спринтами/бэклогом
        if (sprint_id === -1 && current_task_container_name !== 'backlog') {
            $(task).appendTo('#id_backlog_container');
        } else if (sprint_id !== -1 && sprint_id !== +current_task_container_id){
            $(task).appendTo('#id_sprint_id'+sprint_id);
        }
    }

    // смена состояния
    let state_id = +data.task_state.id;
    if (!isNaN(this_sprint_id)) {
        $(task).appendTo('#state_'+state_id);
    } else {
        if (state_id === 5) {
            $(task).find('.task-name').addClass('task_done');
        } else {
            $(task).find('.task-name').removeClass('task_done');
        }
    }

    // смена пользователя
    let task_worker_block = task.find('.task-worker-block');
    if (data.worker_id) {
        if (task_worker_block.length > 0) {
            task_worker_block.attr('href', `/users/${data.worker_id}`)
                .find('img').attr('src', data.avatar);
        } else {
            let task_users_container = task.find('.task-users-container');
            let template = `
                <a href="/users/${data.worker_id}" class="task-worker-block">
                    <span class="uk-text-muted lp-text-smaller">И:</span>
                    <img class="task-user-name uk-margin-small-right" src="${data.avatar}">
                </a>
            `;
            task_users_container.prepend(template);
        }
    } else {
        task_worker_block.remove();
    }
}

function onTaskReportUpdateView(data) {
    data = data[1]; // 0 элемент - сообщение от сервера
    if (!data) return;
    let task = $('#id_task_' + data.id);
    // do nothing
}

function onTaskSubtasksUpdateView(data) {
    data = data[1]; // 0 элемент - сообщение от сервера
    if (!data) return;
    let task = $('#id_task_' + data.id);
}

function onTaskTagsUpdateView(data) {
    data = data[1]; // 0 элемент - сообщение от сервера
    if (!data) return;
    let task = $('#id_task_' + data.id);
    // обновление счётчиков подзадач
    let sprint_task_subtasks = task.find('.task-counter');
    if (sprint_task_subtasks) {
        sprint_task_subtasks.text(`${data.sub}/${data.sub_done}`);
    }
}

function onTaskParentUpdateView(data) {
    // обновление canvas
    $('.uk-tab-left').find('li.uk-active').trigger('click');
}

function onTaskStoryUpdateView(data) {
    // do nothing
}

function onShowTaskDelete(data) {
    if ( !(data = data[1]) ) return;
    $(`#id_task_${data}`).remove();
    initCounters();
}

function onCommentDataShow(data) {
    document.cookie = `comment_hash=${data.hash}; expires=10;`;
    window.location = data.link;
}

function onRemoveReadComment(data) {
    $('#list_comment_item_' + data.result).remove();
    let notify_counter = $('#profile_notify_counter');
    notify_counter.text(notify_counter.text() - 1);
}