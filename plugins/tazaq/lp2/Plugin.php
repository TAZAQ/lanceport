<?php namespace Tazaq\Lp2;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            'Tazaq\Lp2\Components\Categories' => 'categories',
            'Tazaq\Lp2\Components\Project' => 'project',
            'Tazaq\Lp2\Components\Sprint' => 'sprint',
            'Tazaq\Lp2\Components\Tracker' => 'tracker',
            'Tazaq\Lp2\Components\Report' => 'report',
            'Tazaq\Lp2\Components\Task' => 'task',
            'Tazaq\Lp2\Components\Profile' => 'profile'
        ];
    }

    public function registerSettings()
    {
    }
}
