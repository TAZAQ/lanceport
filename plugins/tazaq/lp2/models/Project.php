<?php namespace Tazaq\Lp2\Models;

use Model;
use Exception;
use Tazaq\Lp2\Classes\HelperHistory;
use Db;

/**
 * Model
 */
class Project extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'tazaq_lp2_projects';
    public $timestamps = true;
    protected $slugs = ['slug' => 'name'];

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /* Relations */
    public $belongsTo = [
        'category' => 'Tazaq\Lp2\Models\Category'
    ];

    public $attachOne = [
        'project_image' => 'System\Models\File'
    ];

    public $hasMany = [
        'sprints' => 'Tazaq\Lp2\Models\Sprint',
        'tasks' => 'Tazaq\Lp2\Models\Task',
    ];

    public $belongsToMany = [
        'lpusers' => [
            'Tazaq\Lp2\Models\Lpuser',
            'table' => 'tazaq_lp2_users_projects_access',
            'pivot' => ['acs_project', 'acs_sprint', 'acs_task', 'acs_report', 'acs_tracker']
        ]
    ];

    public $morphMany = [
        'history' => ['Tazaq\Lp2\Models\History', 'name' => 'lp2model']
    ];


    public function afterSave() {
        Db::beginTransaction();

        try {
            $h = new HelperHistory(\Auth::getUser()->id);

            $dict1 = ['name' => 'Название', 'descr' => 'Описание', 'slug' => 'Ссылка',
                'planned_date_start' => 'Планируемая дата начала',
                'planned_date_finish' => 'Планируемая дата завершения', 'real_date_finish' => 'Дата завершения',
                'extra' => 'Экстра'
            ];

            if (!$this->original) {

                $h->set('Категория', $this->category->name);
                $h->add('Новый проект', $this->name);
                $h->set($dict1['slug'], $this->slug);
                $h->set($dict1['descr'], $this->descr ?? '');
                $h->set($dict1['planned_date_start'], $this->planned_date_start);
                $h->set($dict1['planned_date_finish'], $this->planned_date_finish ?? '---Не определено---');
            } else {
                foreach ($this->changes as $field => $value) {
                    switch ($field) {
                        case 'category_id': $h->change('Категория', $this->category->name);
                            break;
                        default:
                            if (in_array($field, array_keys($dict1))) $h->change($dict1[$field], $value);
                            break;
                    }
                }
            }
            if ($history = $h->save()) $this->history()->add( $history );
            Db::commit();
        } catch (Exception $exception) {Db::rollback(); }
    }

    public function beforeDelete() {
        Db::beginTransaction();

        try {
            $h = new HelperHistory(\Auth::getUser()->id);
            $h->delete('Проект', $this->name);
            $this->history()->add( $h->save() );
            Db::commit();
        } catch (Exception $exception) {Db::rollback(); }
    }

    /* Methods */
    public function getCategoryIdOptions($keyValue = null) {
        return Category::lists('name', 'id');
    }

    /**
     * вернет id проекта по slug
     * @param string $slug
     * @return mixed
     */
    public static function getProjectIdBySlug(string $slug) {
        return Project::where('slug', $slug)->first(['id'])->id ?? false;
    }

    /**
     * вернет slug проекта по id
     * @param int $id
     * @return mixed
     */
    public static function getProjectSlugById(int $id) {
        return Project::where('id', $id)->first(['slug'])->slug ?? false;
    }

}
