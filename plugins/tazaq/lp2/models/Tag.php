<?php namespace Tazaq\Lp2\Models;

use Model;

/**
 * Model
 */
class Tag extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'tazaq_lp2_tags';
    public $timestamps = true;

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];


    public $belongsToMany = [
        'tasks' => [
            'Tazaq\Lp2\Models\Task',
            'table' => 'tazaq_lp2_tasks_tags',
            'order' => 'name'
        ]
    ];

}
