<?php namespace Tazaq\Lp2\Models;

use Model;

/**
 * Model
 */
class History extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];
    public $timestamps = true;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'tazaq_lp2_history';

    /**
     * @var array Validation rules
     */
    public $rules = [
        'lpuser_id' => 'required|min:1',
        'data' => 'required'
    ];

    /* Relations */

    public $morphTo = [
        'lp2model' => []
    ];

    public $belongsTo = [
        'lpuser' => 'Tazaq\Lp2\Models\Lpuser',
    ];
}
