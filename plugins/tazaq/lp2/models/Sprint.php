<?php namespace Tazaq\Lp2\Models;

use Exception;
use Model;
use Tazaq\Lp2\Classes\HelperHistory;
use Tazaq\Lp2\Classes\HelperFunctions as HF;
use Db;

/**
 * Model
 */
class Sprint extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'tazaq_lp2_sprints';
    public $timestamps = true;

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /* Relations */
    public $belongsTo = [
        'project' => 'Tazaq\Lp2\Models\Project'
    ];

    public $hasMany = [
        'tasks' => 'Tazaq\Lp2\Models\Task',
    ];

    public $morphMany = [
        'history' => ['Tazaq\Lp2\Models\History', 'name' => 'lp2model']
    ];

    /* Methods */
    public function getProjectIdOptions($keyValue = null) {
        return Project::lists('name', 'id');
    }

    public function afterSave() {
        Db::beginTransaction();

        try {
            $h = new HelperHistory(\Auth::getUser()->id);
            $project_name = Project::with([])->find($this->project_id, ['name'])->name;

            if (!$this->original) {
                $h->add('Новый спринт', $this->name);
                $h->set('Проект', $project_name);
                $h->set('Описание', $this->descr ?? '');
                $h->set('Дата начала', $this->date_start);
                $h->set('Дата окончания', $this->date_finish);
            } else {
                if ($this->name !== $this->original['name']) $h->update('Название', $this->name);
                if ($this->project_id !== $this->original['project_id']) $h->update('Проект', $project_name);
                if ($this->descr !== $this->original['descr']) $h->update('Описание', $this->descr);
                if ($this->date_start !== $this->original['date_start']) $h->update('Дата начала', $this->date_start);
                if ($this->date_finish !== $this->original['date_finish']) $h->update('Дата окончания', $this->date_finish);
            }
            $this->history()->add( $h->save() );
            Db::commit();
        } catch (Exception $exception) {Db::rollback(); }
    }

    public function beforeDelete() {
        Db::beginTransaction();

        try {
            $h = new HelperHistory(\Auth::getUser()->id);
            $h->delete('Спринт', $this->name);
            $this->history()->add( $h->save() );
            Db::commit();
        } catch (Exception $exception) {Db::rollback(); }
    }



    /**
     * Создание спринта с ведением истории
     * @param string $project_slug
     * @param string $name
     * @param array $dates
     * @param string $descr
     * @return bool|Sprint
     */
    public static function createHH(string $project_slug, string $name, array $dates=[], string $descr='Без описания')
    {
        $new_sprint = new Sprint();
        if ($project = Project::where('slug', $project_slug)->first(['id', 'name'])) {
            $new_sprint->project_id = $project->id;
        } else return false;

        $new_sprint->name = $name;
        $new_sprint->descr = $descr;

        $today = HF::today();
        $after_week = HF::after_week();

        if ($dates) {

            if (HF::is_Date($dates[0])) { $new_sprint->date_start = $dates[0]; } else { $new_sprint->date_start = $today; }
            if (HF::is_Date($dates[1])) { $new_sprint->date_finish = $dates[1]; } else { $new_sprint->date_finish = $after_week; }
        } else {
            $new_sprint->date_start = $today;
            $new_sprint->date_finish = $after_week;
        }

        try {
            $new_sprint->save();
        } catch (Exception $e) { return false; };

        return $new_sprint;
    }
}
