<?php namespace Tazaq\Lp2\Models;

use Model;
use Pheanstalk\Exception;
use Tazaq\Lp2\Classes\HelperHistory;
use Db;

/**
 * Model
 */
class Task extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    use \October\Rain\Database\Traits\SimpleTree;

    protected $dates = ['deleted_at'];

    const PARENT_ID = 'task_id';


    /**
     * @var string The database table used by the model.
     */
    public $table = 'tazaq_lp2_tasks';
    public $timestamps = true;

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public const MODEL_NAMESPACE = 'Tazaq\Lp2\Models\Task';

    /* Relations */
    public $belongsTo = [
        'project' => 'Tazaq\Lp2\Models\Project',
        'task' => 'Tazaq\Lp2\Models\Task',
        'lpuser' => 'Tazaq\Lp2\Models\Lpuser',
        'worker' => 'Tazaq\Lp2\Models\Lpuser',
        'task_type' => 'Tazaq\Lp2\Models\Task_type',
        'task_priority' => 'Tazaq\Lp2\Models\Task_priority',
        'parent'    => ['Task', 'key' => self::PARENT_ID],
        'sprint' => 'Tazaq\Lp2\Models\Sprint',
        'task_state' => 'Tazaq\Lp2\Models\Task_state',
    ];

    public $belongsToMany = [
        'tags' => [
            'Tazaq\Lp2\Models\Tag',
            'table' => 'tazaq_lp2_tasks_tags',
            'order' => 'name'
        ]
    ];

    public $hasMany = [
        'comments' => 'Tazaq\Lp2\Models\Comment',
        'children'    => ['Task', 'key' => self::PARENT_ID],
    ];

    public $attachMany = [
        'photos' => 'System\Models\File'
    ];

    public $morphMany = [
        'history' => ['Tazaq\Lp2\Models\History', 'name' => 'lp2model']
    ];

    /* Methods */
    public function getProjectIdOptions($keyValue = null) {
        return Project::orderBy('name')->lists('name', 'id');
    }

    public function getTaskIdOptions($keyValue = null) {
        return Task::orderBy('name')->lists('name', 'id');
    }

    public function getLpuserIdOptions($keyValue = null) {
        return Lpuser::orderBy('name')->lists('name', 'id');
    }

    public function getTaskTypeIdOptions($keyValue = null) {
        return Task_type::orderBy('name')->lists('name', 'id');
    }

    public function getTaskPriorityIdOptions($keyValue = null) {
        return Task_priority::orderBy('level')->lists('name', 'id');
    }

    public function getSprintIdOptions($keyValue = null) {
        return Sprint::where('project_id', $this->project_id)->orderBy('name')->lists('name', 'id');
    }

    public function getTaskStateIdOptions($keyValue = null) {
        return Task_state::orderBy('id')->lists('name', 'id');
    }



    public function afterSave() {
        // подзадачи не трогаем
        if ($this->task_type_id == 2) return;

        Db::beginTransaction();

        try {
            $h = new HelperHistory(\Auth::getUser()->id);

            if (!$this->original) {
                $h->set('Проект', $this->project->name);
                $h->set('Тип задачи', $this->task_type->name);
                $h->set('Приоритет', $this->task_priority->name );
                $h->set('Создал', $this->lpuser->name);
                if ($this->sprint) {
                    $h->set('Спринт', $this->sprint->name);
                }
                $h->set('Состояние', $this->task_state->name);

                $h->add('Новая задача', $this->name);
                $h->set('Описание', $this->descr ?? '');
                $h->set('Затраты', $this->estimate ?? 1);
                $h->set('Отслеживание', $this->is_tracked ? 'Да' : 'Нет');

            } else {
                $dict1 = ['name' => 'Название', 'descr' => 'Описание' , 'estimate' => 'Затраты', 'result_text' => 'Отчёт', 'extra' => 'Экстра'];
                $dict2 = ['in_report' => 'В отчёте', 'is_tracked' => 'Отслеживается', 'is_closed' => 'Закрыта'];

                foreach ($this->changes as $field => $value) {
                    switch ($field) {
                        case 'task_type_id': $h->change('Тип задачи', $this->task_type->name);
                            break;
                        case 'task_priority_id': $h->change('Тип задачи', $this->task_priority->name);
                            break;
                        case 'task_id':
                            if ( $this->original[$field] && $value ) $h->change('Задача-родитель', $this->task->name);
                            else if ( $this->original[$field] && !$value ) $h->delete('Задача-родитель', '---Корень---');
                            else if ( !$this->original[$field] && $value ) $h->set('Задача-родитель', $this->task->name);
                            break;
                        case 'worker_id':
                            if ( $this->original[$field] && $value ) { $h->change('Исполнитель', $this->worker->name);}
                            else if ( $this->original[$field] && !$value ) $h->delete('Исполнитель', '---Без исполнителя---');
                            else if ( !$this->original[$field] && $value ) $h->set('Исполнитель', $this->worker->name);
                            break;
                        case 'sprint_id':
                            if ( $this->original[$field] && $value ) $h->move('Спринт', $this->sprint->name);
                            else if ( $this->original[$field] && !$value ) $h->delete('Спринт', '---Без спринта---');
                            else if ( !$this->original[$field] && $value ) $h->move('Спринт', $this->sprint->name);  //повторение для красоты
                            break;
                        case 'task_state_id': $h->move('Состояние', $this->task_state->name);
                            break;
                        default:
                            if (in_array($field, array_keys($dict1))) $h->change($dict1[$field], $value);
                            elseif (in_array($field, array_keys($dict2))) $h->change($dict2[$field], $value ? 'Да' : 'Нет');
                            break;
                    }
                }
            }
            if ($history = $h->save()) $this->history()->add( $history );
            Db::commit();
        } catch (Exception $exception) {Db::rollback(); }
    }

    public function beforeDelete() {
        Db::beginTransaction();

        try {
            $h = new HelperHistory(\Auth::getUser()->id);
            $h->delete('Задача', $this->name);
            $this->history()->add( $h->save() );
            Db::commit();
        } catch (Exception $exception) {Db::rollback(); }
    }

    public function afterAttach(string $relationName, array $attachedIdList, array $insertData) {
        dd($relationName);
    }

    /**
     * вернёт количество подзадач и их готовность
     * @param $task_id
     * @return array
     */
    public static function getSubtasksCounts($task_id) {
        $task_subs = Task::where('task_id', $task_id)
            ->where('task_type_id', 2)
            ->get();
        $task_sub = count($task_subs);
        $task_sub_done = 0;
        foreach ($task_subs as $sub) {
            if ($sub->task_state_id == 5) $task_sub_done++;
        }

        return [
            'sub' => $task_sub,
            'sub_done' => $task_sub_done
        ];
    }

    /**
     * дерево задач
     * @return mixed
     */
    public static function getTaskTree() {
        return Task::with(['parent', 'children'])
            ->orderBy('name')
            ->where('task_type_id', '<>', 2)
            ->get(['id', 'name', 'task_type_id', 'task_id'])
            ->toNested();
    }

    public static function onTaskSprintMoveHH(int $task_id, int $user_id, array $sprint_data) {
        $h = new HelperHistory($user_id);
        $task = Task::where('id', $task_id)->first(['id', 'sprint_id']);
        if ($task->sprint_id != $sprint_data['id']) {
            $task->sprint_id = $sprint_data['id'];
            $h->move('Спринт', $sprint_data['name']);

            try { $task->save(); } catch (Exception $e) { return false; }
            if ($history = $h->save() ) {
                $task->history()->add( $history );
            } else return false;

            return true;
        } else return true;
    }
}
