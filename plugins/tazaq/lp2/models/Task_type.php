<?php namespace Tazaq\Lp2\Models;

use Model;

/**
 * Model
 */
class Task_type extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'tazaq_lp2_task_types';
    public $timestamps = true;

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
