<?php namespace Tazaq\Lp2\Models;

use Model;

/**
 * Model
 */
class Access extends Model
{
    use \October\Rain\Database\Traits\Validation;

    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'tazaq_lp2_users_projects_access';
    public $timestamps = true;

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /* Relations */
    public $belongsTo = [
        'lpuser' => 'Tazaq\Lp2\Models\Lpuser',
        'project' => 'Tazaq\Lp2\Models\Project',
    ];

    /* Methods */
    public function getLpuserIdOptions($keyValue = null) {
        return Lpuser::orderBy('name')->lists('name', 'id');
    }

    public function getProjectIdOptions($keyValue = null) {
        return Project::orderBy('name')->lists('name', 'id');
    }
}
