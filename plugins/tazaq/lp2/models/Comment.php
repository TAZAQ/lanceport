<?php namespace Tazaq\Lp2\Models;

use Model;

/**
 * Model
 */
class Comment extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    use \October\Rain\Database\Traits\SimpleTree;


    protected $dates = ['deleted_at'];

    const PARENT_ID = 'comment_id';


    /**
     * @var string The database table used by the model.
     */
    public $table = 'tazaq_lp2_tasks_lpusers_comments';
    public $timestamps = true;

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /* Relations */
    public $belongsTo = [
        'task' => 'Tazaq\Lp2\Models\Task',
        'lpuser' => 'Tazaq\Lp2\Models\Lpuser',
        'parent'    => ['Comment', 'key' => self::PARENT_ID],
    ];

    public $hasMany = [
        'children'    => ['Comment', 'key' => self::PARENT_ID],
    ];

    /* Methods */
    public function getTaskIdOptions($keyValue = null) {
        return Task::orderBy('name')->lists('name', 'id');
    }

    public function getLpuserIdOptions($keyValue = null) {
        return Lpuser::orderBy('name')->lists('name', 'id');
    }
}
