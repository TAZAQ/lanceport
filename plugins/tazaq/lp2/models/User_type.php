<?php namespace Tazaq\Lp2\Models;

use Model;

/**
 * Model
 */
class User_type extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'tazaq_lp2_user_types';
    public $timestamps = true;

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
