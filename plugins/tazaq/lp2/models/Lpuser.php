<?php namespace Tazaq\Lp2\Models;

use Model;
use RainLab\User\Models\User;

/**
 * Model
 */
class Lpuser extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'tazaq_lp2_lpusers';
    public $timestamps = true;

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /* Relations */
    public $belongsTo = [
        'user' => 'Rainlab\User\Models\User',
        'user_type' => 'Tazaq\Lp2\Models\User_type',
    ];

    public $hasOne = [
        'access' => 'Tazaq\Lp2\Models\Access',
    ];

    public $belongsToMany = [
        'projects' => [
            'Tazaq\Lp2\Models\Project',
            'table' => 'tazaq_lp2_users_projects_access',
            'order' => 'tazaq_lp2_projects.name'
        ],
        'comments' => [
            'Tazaq\Lp2\Models\Comment',
            'table' => 'tazaq_lp2_tasks_lpusers_comments',
            'order' => 'tazaq_lp2_tasks_lpusers_comments.updated_at'
        ]
    ];

    public $attachOne = [
        'avatar' => 'System\Models\File'
    ];

    /* Methods */
    public function getUserIdOptions($keyValue = null) {
        $names = User::lists('name', 'id');
        $surnames = User::lists('surname', 'id');
        $usernames = User::orderBy('surname')->lists('username', 'id');

        $surname_name = [];
        foreach ($usernames as $key => $value) {
            $surname_name["$key"] = $surnames[$key] . ' ' . $names[$key] . ':  ' . $value;
        }

        return $surname_name;
    }

    public function getUserTypeIdOptions($keyValue = null) {
        return User_type::lists('name', 'id');
    }

    /**
     * Возвращает lpuser_id по user_id
     * @param $user_id
     * @return integer|null
     */
    public static function getLpuserIdByUserId($user_id) {
        return Lpuser::where('user_id', $user_id)->first(['id'])->id ?? null;
    }

}
