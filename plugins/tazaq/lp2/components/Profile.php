<?php
namespace Tazaq\Lp2\Components;

use Cms\Classes\ComponentBase;
use RainLab\User\Models\User;
use Tazaq\Lp2\Classes\HelperFunctions;
use Tazaq\Lp2\Models\Comment;
use Tazaq\Lp2\Models\Lpuser;
use Tazaq\Lp2\Models\Project as Project;
use Auth;
use Tazaq\Lp2\Components\AccessChecker as AC;
use System\Models\File as FileModel;
use Input;


class Profile extends ComponentBase {

    public $lpuser;
    public $lpuser_projects;
    public $lpuser_comments;


    /**
     * @return string[]
     */
    public function componentDetails()
    {
        return [
            'name' => 'Профиль пользователя',
            'description' => 'Настройки пользователя'
        ];
    }


    /**
     * свойства компонента
     * @return array
     */
    public function defineProperties()
    {
        return [
            'id' => [
                'title' => 'id пользователя',
                'description' => 'id пользователя',
                'default' => '{{ :id }}',
                'type' => 'string',
            ],
            'slug' => [
                'title' => 'slug проекта',
                'description' => 'slug проекта',
                'default' => '{{ :slug }}',
                'type' => 'string',
            ],
        ];
    }


    /**
     * загрузка компонента
     */
    public function onRun() {
        $this->lpuser = $this->onLpuserLoad();
        $this->lpuser_projects = $this->onLpuserProjectsLoad();
        $this->lpuser_comments = $this->onLpuserCommentsLoad();
    }

    private function onLpuserLoad() {
        $project_id = null;
        if ( $slug = $this->property('slug') ) {
            $project_id = Project::where(['slug' => $slug])->first()->id;
        }

        // профиль
        if ( ($id = $this->property('id')) && !$project_id) {
            return Lpuser::with([
                'user_type',
                'user',
                'access' => function($q) use ($project_id) {
                    $q->where('project_id', $project_id);
                }
            ])->find($id);
        } else if ($user = Auth::getUser()) {  // не профиль
            return Lpuser::with([
                'user_type',
                'user',
                'access' => function($q) use ($project_id) {
                    $q->where('project_id', $project_id);
                }
            ])->where(['user_id' => $user->id])->first();
        }
        return null;
    }

    /**
     * Вывод всех проектов с задачами (не выполненными), в которых участвует пользователь
     * @return mixed|null
     */
    private function onLpuserProjectsLoad() {
        if ( $id = $this->property('id') ) {

            return Lpuser::with([
                'projects' => function($q) {
                    $q->where(AC::ACS_PRJ, '>=', AC::IS_R)
                        ->orderBy('name')
                        ->select(['tazaq_lp2_projects.id', 'name', 'slug']);
                },
                'projects.tasks' => function($q) use ($id) {
                    $q->select(['id', 'name', 'project_id', 'task_type_id', 'task_priority_id', 'sprint_id',
                        'worker_id', 'task_state_id', 'is_tracked'])
                        ->where('task_type_id', '<>', 2)
                        ->where('task_state_id', '<>', '5')
                        ->orderBy('name')
                        ->where(function ($q1) use ($id) {
                            $q1->where('worker_id', $id)
                                ->orWhere('lpuser_id', $id, 'user_type_id', 2);
                        })
                    ;
                },
                'projects.tasks.task_priority' => function($q) {
                    $q->select(['id', 'css']);
                },
                'projects.tasks.task_type' => function($q) {
                    $q->select(['id', 'css']);
                },
            ])
                ->where('id', $id)
                ->first(['id', 'user_type_id']);
        }
        return null;
    }

    /**
     * Непрочитанные комментарии
     * @return mixed|null
     */
    private function onLpuserCommentsLoad() {
        if ( $id = $this->property('id') ) {

            $model = Comment::with([
                'parent',
                'children' => function($q) use($id) {
                    $q->where('lpuser_id', '<>', $id);
                },
                'lpuser',
                'task.project'
            ])
                ->where('is_read', $id)
                ->where('lpuser_id', '<>', $id)
                ->get();

            return $model;
        }

        return null;
    }

    /**
     * Установка отметки "прочитано"
     * @return array|mixed|\October\Rain\Flash\FlashBag
     */
    public function onCommentRead() {
        if (!Auth::getUser()) return \Flash::error('Вы не авторизованы!');
        if (!$data = post()) return  \Flash::error('Неверное количество параметров');

        $comment = Comment::find($data['id']);
        $comment->is_read = null;
        $comment->save();

        return $data['id'];
    }


    public function onLpuserDataSave() {
        $data = post();
        if ( !Auth::getUser() ) return \Redirect::to('/');
        if ( !$lpuser = Lpuser::where('id', $data['id'])->where('user_id', Auth::getUser()->id)->first() ) return \Redirect::to('/');

        HelperFunctions::Validate($data, ['name' => 'required|min:1']);

        $lpuser->name = $data['name'];
        $lpuser->initials = $data['initials'] ?: mb_strtoupper(mb_substr($data['name'], 0, 2));
        $lpuser->social = $data['social'] ?: '';
        $lpuser->about = $data['about'] ?: '';
        $lpuser->save();


        if ($image = Input::file('avatar')) {
            if ($image->getFileInfo()->getSize() > 100000) { return \Flash::error('Изображение не должно превышать 100 КБ'); }
            $file = new FileModel;
            $file->data = $image;
            $file->is_public = true;
            $file->save();
            $lpuser->avatar()->add( $file );
        }

        return \Redirect::refresh();
    }

    public function onRealUserDataSave() {
        $data = post();
        if ( !Auth::getUser() ) return \Redirect::to('/');
        if ( !$user = User::find(Auth::getUser()->id) ) return \Redirect::to('/');

        HelperFunctions::Validate($data, [
            'username' => 'required|min:1',
            'name' => 'required|min:1',
            'surname' => 'required|min:1',
            'email' => 'required|min:1',
        ]);

        $user->username = $data['username'];
        $user->name = $data['name'];
        $user->surname = $data['surname'];
        $user->email = $data['email'];

        if (\Hash::check($data['oldpass'], $user->password)) {
            if (\Hash::make($data['newpass']) === \Hash::make($data['newpass2'])) {
                $user->password = Hash::make('newpass');
            }
        }

        $user->save();

        return \Redirect::refresh();
    }

}