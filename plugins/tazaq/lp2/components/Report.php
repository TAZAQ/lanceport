<?php
namespace Tazaq\Lp2\Components;

use Cms\Classes\ComponentBase;
use Tazaq\Lp2\Components\AccessChecker as AC;
use Tazaq\Lp2\Models\Project as Project;
use Tazaq\Lp2\Models\Task;
use Tazaq\Lp2\Models\Task_state;


class Report extends ComponentBase {

    public $report;


    /**
     * @return string[]
     */
    public function componentDetails()
    {
        return [
            'name' => 'Лента отчёта',
            'description' => 'Вывод задач в ленту'
        ];
    }


    /**
     * свойства компонента
     * @return array
     */
    public function defineProperties()
    {
        return [
            'slug' => [
                'title' => 'Slug проекта',
                'description' => 'Slug проекта',
                'default' => '{{ :slug }}',
                'type' => 'string',
            ],
        ];
    }


    /**
     * загрузка компонента
     */
    public function onRun() {
        if (!$this->report = $this->loadReport()) {
            return $this->controller->run('403');
        }
    }


    /**
     * Загрузка задач отчёта
     * @return mixed
     */
    private function loadReport() {
        if ( $slug = $this->property('slug') ) {
            if (!AC::onLoadCheckAccess($slug, AC::ACS_REP)) return false;
            $model = Task::with(['task_type', 'task_priority', 'tags', 'lpuser.avatar'])
                ->where('in_report', '<>', null)
                ->where('project_id', Project::getProjectIdBySlug($slug))
                ->orderByDesc('updated_at')
                ->get();

            return $model;
        }
    }
}