<?php
namespace Tazaq\Lp2\Components;

use Cms\Classes\ComponentBase;
use Tazaq\Lp2\Components\AccessChecker as AC;
use Tazaq\Lp2\Models\Project as Project;
use Tazaq\Lp2\Models\Tag;
use Tazaq\Lp2\Models\Task;
use Tazaq\Lp2\Models\Task_state;

class Tracker extends ComponentBase {


    public $tracker_info;


    /**
     * @return string[]
     */
    public function componentDetails()
    {
        return [
            'name' => 'Трекер задач',
            'description' => 'Вывод задач в трекере'
        ];
    }


    /**
     * свойства компонента
     * @return array
     */
    public function defineProperties()
    {
        return [
            'slug' => [
                'title' => 'Slug проекта',
                'description' => 'Slug проекта',
                'default' => '{{ :slug }}',
                'type' => 'string',
            ],
        ];
    }


    /**
     * загрузка компонента
     */
    public function onRun() {
        if (!$this->tracker_info = $this->loadTracker()) {
            return $this->controller->run('403');
        }
    }


    /**
     * Загрузка задач трекера
     * @return mixed
     */
    private function loadTracker() {
        if ( $slug = $this->property('slug') ) {
//            dd(AC::onLoadCheckAccess($slug, AC::ACS_TRC));
            if (!AC::onLoadCheckAccess($slug, AC::ACS_TRC)) return false;

            $page = get('page') ?? 1;


            $project_id = Project::getProjectIdBySlug($slug);
            $model = Task::with(['task_type', 'task_priority', 'tags', 'lpuser'])
                ->where('is_tracked', 1)
                ->where('project_id', $project_id)
                ->orderByDesc('updated_at')
                ->paginate(50, $page);

            $model_data['tasks'] = $model;
            $model_data['states_count'] = Task_state::count();

            return $model_data;
        }
    }
}