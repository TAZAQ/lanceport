<?php
namespace Tazaq\Lp2\Components;

use Cms\Classes\ComponentBase;
use Guzzle\Plugin\History\HistoryPlugin;
use mysql_xdevapi\Exception;
use Tazaq\Lp2\Classes\HelperFunctions as HF;
use Tazaq\Lp2\Models\Category;
use Tazaq\Lp2\Models\Lpuser;
use Tazaq\Lp2\Models\Tag;
use Tazaq\Lp2\Models\History;
use Tazaq\Lp2\Models\Task_priority;
use Tazaq\Lp2\Models\Project as ProjectModel;
use Tazaq\Lp2\Models\Sprint;
use Tazaq\Lp2\Models\Task;
use Tazaq\Lp2\Models\Task_state;
use Tazaq\Lp2\Models\Task_type;
use Flash;
use Tazaq\Lp2\Components\AccessChecker as AC;
use Input;
use System\Models\File as FileModel;
use Tazaq\Lp2\Classes\HelperHistory;
use Auth;

class Project extends ComponentBase
{

    /**
     * @var Collection проект для отображения
     */
    public $project;

    /**
     * @var Collection спринты для отображения
     */
    public $sprints;

    /**
     * @var Collection задачи для отображения
     */
    public $tasks;

    public $all_categories;

    /**
     * Описание компонента
     */
    public function componentDetails()
    {
        return [
            'name' => 'Проект',
            'description' => 'Вывод содержимого проектов'
        ];
    }


    /**
     *  Поля компонента
     */
    public function defineProperties()
    {
        return [
            'slug' => [
                'title' => 'Url',
                'description' => 'Url адрес проекта',
                'default' => '{{ :slug }}',
                'type' => 'string',
            ],
        ];
    }

    /**
     * записывает в 'project' project
     * записывает в 'sprints' sprints
     * записывает в 'tasks' tasks
     */
    public function onRun()
    {
        if ($this->project = $this->loadProject()) {
            $this->sprints = $this->loadSprints($this->project->id);
            $this->tasks = $this->loadTasks($this->project->id);
            $this->all_categories = $this->loadAllCategories();
        } else {
            return $this->controller->run('403');
        }
    }


    /**
     * @return mixed вернет текущий проект
     */
    protected function loadProject()
    {
        if ($slug = $this->property('slug')) {
            // проверка на доступ
            if (!AC::onLoadCheckAccess($slug)) {
                return false;
            }

            return ProjectModel::where('slug', $slug)->first();
        } else {
            return false;
        }
    }

    private function loadSprints($project_id)
    {
        $sprints = Sprint::with(['tasks' => function($q) { $q->orderBy('name'); }])
            ->where('project_id', $project_id)
            ->orderByDesc('date_finish')
            ->orderByDesc('date_start')
            ->get();

        return $sprints;
    }

    private function loadTasks($project_id)
    {
        $tasks = Task::with(['task_state', 'task_priority', 'lpuser.avatar', 'sprint'])
            ->where('task_type_id', '<>', 2)
            ->where('tazaq_lp2_tasks.deleted_at', null)
            ->where('project_id', $project_id)
            ->orderBy('name', 'desc')
            ->get();

        return $tasks;
    }

    private function loadAllCategories(){
        return Category::get(['id', 'name']);
    }

    /**
     * Вернёт id проекта по slug     ...или нет
     * @param string $project_slug
     * @return mixed
     */
    private function getProjectIdBySlug(string $project_slug) {
        return ProjectModel::where('slug', $project_slug)->first(['id'])->id;
    }


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * AJAX * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    const DATA_TASK_ID = 'task_id';

    /**
     * метод для добавления задачи в бэклог
     * @return array|null
     */
    public function onTaskAdd() {
        $data = post();

        $rules = [
            'project' => 'required|min:1',
            'task_name' => 'required|min:5|max:100',
            'sprint' => 'numeric'
        ];

        /* Валидация */
        HF::Validate($data, $rules);

        // проект
        $slug = post('project');
        $project_id = ProjectModel::where('slug', $slug)->first(['id'])->id;


        /* Проверка доступа */
        $real_user_id = $data['user'];
        $access = AC::check($project_id, $real_user_id, [AC::ACS_TSK]);
        if (! $access) {
            Flash::error(AC::ERR_NO_ACCESS);
            return null;
        }

        // пользователь
        $lpuser_id = Lpuser::where('user_id', $real_user_id)->first(['id'])->id;


        // добавление задачи
        if ($project_id) {
            $task_type_id = Task_type::where('name', 'Задача')->first(['id'])->id;
            $priority_id = Task_priority::where('name', 'Обычный')->first(['id'])->id;

            $name = post('task_name');

            // создание задачи
            $task = new Task;
            $task->project_id = $project_id;
            $task->lpuser_id = $lpuser_id;
            $task->task_type_id = $task_type_id;
            $task->task_priority_id = $priority_id;
            $task->task_state = 1;
            $task->sprint_id = $data['sprint'] == "" ? null : $data['sprint'];
            $task->name = $name;
            $task->is_tracked = $data['tracker'] == "1";
            $task->save();

            if ($data['tracker'] == "1") return \Redirect::refresh();

            // если задача добавлена
            if ($task) {
                $task = [
                    'id' => $task->id,
                    'name' => $task->name,
                    'css' => $task->task_priority->css,
                    'type' => $task->task_type->id,
                    'created_at' => $task->created_at,
                    'priority' => $task->task_priority->level,
                    'worker' => $task->lpuser_id
                ];

                Flash::success("Задача `{$name}` добавлена");
                return $task;
            } else {
                Flash::error('Ошибка при создании задачи');
                return null;
            }
        } else {
            Flash::error('Ошибка при добавлении, проекта не существует');
            return null;
        }
    }




    /**
     * добавление/удаление задачи из спринта
     * @return |null
     */
    public function onSprintTaskMoved() {
        $data = post();
        $rules = [
            'project_id' => 'required|min:1',
            'user_id' => 'required|min:1',
            'task_id' => 'required|min:1',
            'sprint_id' => 'required|min:1',
        ];


        /* Валидация */
        HF::Validate($data, $rules);


        /* Проверка доступа */
        $project_id = $data['project_id'];
        $real_user_id = $data['user_id'];
        $access = AC::check($project_id, $real_user_id, [AC::ACS_TSK, AC::ACS_SPR]);
        if (! $access) {
            Flash::error(AC::ERR_NO_ACCESS);
            return null;
        }

        $task = Task::find($data['task_id']);
        if ($data['sprint_id'] && ($task->sprint_id != $data['sprint_id']) ) {
            if ($data['sprint_id'] > 0) {
                $task->sprint_id = $data['sprint_id'];
            } else {
                $task->sprint_id = null;
            }
        }
        $task->save();
    }


    /**
     * создание спринта
     * @return Sprint|null
     */
    public function onSprintAdd() {
        $data = post();

        $rules = [
            'project' => 'required|min:1',
            'user' => 'required|min:1',
            'sprint_name' => 'required|min:1',
            'sprint_descr' => 'required|min:1',
            'sprint_start' => 'required|min:1|date',
            'sprint_finish' => 'required|min:1|date',
        ];

        /* Валидация */
        HF::Validate($data, $rules);


        // проверка доступа
        $project_id = $this->getProjectIdBySlug($data['project']);
        $access = AC::check($project_id, $data['user'], [AC::ACS_SPR]);
        if (! $access) {
            Flash::error(AC::ERR_NO_ACCESS);
            return null;
        }

        // создание спринта
        $sprint = new Sprint();
        $sprint->project_id = $project_id;
        $sprint->name = $data['sprint_name'];
        $sprint->descr = $data['sprint_descr'];
        $sprint->date_start = $data['sprint_start'];
        $sprint->date_finish = $data['sprint_finish'];
        $sprint->save();

        $result = $sprint;
        $result['project_slug'] = $data['project'];

        return $result;
    }

    /**
     * @deprecated
     * метод удаления задачи
     * @return array|null
     */
    public function onTaskDelete() {
        $data = post();

        $rules = [
            'project_slug' => 'required|min:1',
            'user_id' => 'required|min:1',
            'task_id' => 'required|min:1'
        ];

        /* Валидация */
        HF::Validate($data, $rules);

        if (!AC::onLoadCheckAccess($data['project_slug'], AC::ACS_TSK, AC::IS_RWD, $data['user_id'])) {
            return Flash::error(AC::ERR_NO_ACCESS);
        }

        $task = Task::find($data['task_id']);
        $task->delete();

        return [Flash::success('Задача успешно удалена'), $data['task_id']];
    }

    /**
     * Сохранение настроек проекта
     * @return array|\October\Rain\Flash\FlashBag
     * @throws \ImagickException
     * @throws \ValidationException
     */
    public function onProjectSave() {
        HF::Validate(post(), HF::ACCESS_RULES);
        $data = post();
        if (!AC::onLoadCheckAccess($data['project_slug'], AC::ACS_PRJ, AC::IS_OWNER, $data['user_id'])) {
            return Flash::error(AC::ERR_NO_ACCESS);
        }

        $rules = [
            'name' => 'required|min:1',
            'planned_date_start' => 'required|date',
            'category' => 'required|min:1'
        ];

        HF::Validate($data, $rules);

        $project = ProjectModel::where('slug', $data['project_slug'])->first();
        $project->name = $data['name'];
        $project->category_id = $data['category'];
        $project->planned_date_start = $data['planned_date_start'];
        $project->descr = $data['description'];
        $project->planned_date_finish = $data['planned_date_finish'];
        if ($data['is_done'] && $data['is_done'] == 'on') $project->real_date_finish = date('Y-m-d H:i:s');
        else $project->real_date_finish = 0;
        $project->extra = $data['extra'];
        $project->save();

        // если есть файл - изображение
        $real_path = '';
        if (Input::hasFile('project_image')) {
            $real_path = Input::file('project_image')->getRealPath();
            HF::resizeImage($real_path);
            $file = new FileModel;
            $file->data = Input::file('project_image');
            $file->is_public = true;
            $file->save();

            try { $project->project_image()->add($file); }
            catch (Exception $e) { return Flash::error('Не удалось прикрепить изображение'); }

            $h = new HelperHistory($data['user_id']);
            $h->update('Изображение', $project->project_image->path);
            // запись в историю
            if ($history = $h->save()) {
                try { $project->history()->add( $history ); } catch (Exception $e) {}
            }
        }

        $project = ProjectModel::with(['project_image'])
            ->where('slug', $data['project_slug'])
            ->first(['id', 'name', 'descr']);

        return [Flash::success('Проект обновлён'), [
            'id' => $project->id,
            'name' => $project->name,
            'descr' => $project->descr,
            'img_path' => $project->project_image ? $project->project_image->path : null
        ]];
    }

    /******************************************************************************************************************/
    /*************Получение данных для отправки в редактор задач*******************************************************/
    /******************************************************************************************************************/

    /**
     * валидация для редактора задач
     * @param $data
     * @return mixed
     * @throws \ValidationException
     */
    private function taskEditorValidating($data) {
        /* валидация */
        $available_methods = [
            'onGetTaskMainData',
            'onGetTaskStatusData',
            'onGetTaskReportData',
            'onGetTaskSubtasksData',
            'onGetTaskTagsData',
            'onGetTaskParentData',
            'onGetTaskStoryData',
        ];

        $available_methods_count = count($available_methods);
        $rules = HF::DEFAULT_RULES;
        $rules['method_index'] = "required|min:0|max:{$available_methods_count}|numeric";
        HF::Validate($data, $rules);
        return $data;
    }

    /**
     * проверка доступа
     * @param $data
     * @return bool|mixed
     */
    private function taskEditorAccessChecking($data) {
        /* проверка доступа */
        $slug = $data['project_slug'];
        return AC::onLoadCheckAccess($slug, AC::ACS_TSK, AC::IS_RW);
    }

    /**
     * Получение данных задачи
     * Узел для перехода по методам
     * @return array|\October\Rain\Flash\FlashBag
     * @throws \ValidationException
     */
    public function onGetTaskData_TE2() {
        $data = $this->taskEditorValidating(post());
        $access = $this->taskEditorAccessChecking($data);
        if (!$access) return Flash::error(AC::ERR_NO_ACCESS);

        switch ($data['method_index']) {
            default: return $this->onGetTaskMainData($data['task_id']); break;
            case 1: return $this->onGetTaskStatusData($data['task_id']); break;
            case 2: return $this->onGetTaskReportData($data['task_id']); break;
            case 3: return $this->onGetTaskSubtasksData($data['task_id']); break;
            case 4: return $this->onGetTaskTagsData($data['task_id']); break;
            case 5: return $this->onGetTaskParentData($data['task_id']); break;
            case 6: return $this->onGetTaskStoryData(); break;
        }
    }

    /**
     * Возвращает основныее поля редактору
     * @param $task_id - идентификатор задачи
     * @return mixed
     */
    private function onGetTaskMainData($task_id) {
        return Task::where('id', $task_id)
            ->first(['name', 'estimate', 'descr', 'extra', 'is_closed']);
    }

    /**
     * Возвращает поля статуса редактору
     * @param $task_id - идентификатор задачи
     * @return array
     */
    private function onGetTaskStatusData($task_id) {
        // данные по задаче
        $fast_q = function ($q) { $q->select(['id', 'name']); };
        $task = Task::with([
            'worker' => $fast_q,
            'task_state' => $fast_q,
            'task_type' => $fast_q,
            'task_priority' => $fast_q,
            'sprint' => $fast_q
        ])->find($task_id);

        // все данные из необходимых таблиц
        $all_sprints = Sprint::where('project_id', $task->project_id)->get(['id', 'name']);
        $all_task_states = Task_state::where('is_visible', 1)->get(['id', 'name']);
        $all_task_types = Task_type::where('id', '<>', 2)->get(['id', 'name']);
        $all_priorities = Task_priority::orderByDesc('level')->get(['id', 'name', 'css']);
        $all_project_users = ProjectModel::where('id', $task->project_id)->with('lpusers')->first()->lpusers;

        // текущие значения связанных таблиц
        $task_data = [];
        $task_data['worker'] = $task->worker_id;
        $task_data['task_state'] = $task->task_state->id;
        $task_data['task_type'] = $task->task_type->id;
        $task_data['task_priority'] = $task->task_priority->id;
        $task_data['sprint'] = $task->sprint;

        // преобразование collection[] -> [id, name]
        $lpusers = [];
        foreach ($all_project_users as $user) $lpusers[] = ['id' => $user->id, 'name' => $user->name];

        $task_data['all_priorities'] = $all_priorities;
        $task_data['all_project_users'] = $lpusers;
        $task_data['all_sprints'] = $all_sprints;
        $task_data['all_task_states'] = $all_task_states;
        $task_data['all_task_types'] = $all_task_types;

        return $task_data;
    }

    /**
     * Отчёт
     * @param $task_id - идентификатор задачи
     * @return mixed
     */
    private function onGetTaskReportData($task_id) {
        return Task::where('id', $task_id)->first(['in_report', 'result_text']);
    }

    /**
     * подзадачи
     * @param $task_id - идентификатор задачи
     * @return mixed
     */
    private function onGetTaskSubtasksData($task_id) {
        $subtasks = Task::where('task_type_id', 2)
            ->where('task_id', $task_id)
            ->get(['id', 'name', 'task_state_id']);

        return $subtasks;
    }

    /**
     * теги
     * @param $task_id - идентификатор задачи
     * @return array
     */
    private function onGetTaskTagsData($task_id) {
        $tags = Task::with('tags')->where('id', $task_id)->first(['id'])->tags;
        // преобразование collection[] -> [id, name]
        $tags_data = [];
        foreach ($tags as $tag) $tags_data[] = ['id' => $tag->id, 'name' => $tag->name];

        return $tags_data;
    }

    /**
     * родители
     * @param $task_id - идентификатор задачи
     * @return array
     */
    private function onGetTaskParentData($task_id) {
        $task = Task::with('task')->find($task_id);
        $parent = $task->task;
        $all_project_tasks = Task::where('project_id', $task->project_id)
            ->where('id', '<>', $task_id)
            ->where('task_type_id', '<>', 2)
            ->orderBy('name')
            ->get(['id', 'name']);

        $all_tasks = [];
        foreach ($all_project_tasks as $t) {$all_tasks[] = ['id' => $t->id, 'name' => $t->name];}

        $parent_data['parent'] = $parent ? ['id' => $parent->id, 'name' => $parent->name] : null;
        $parent_data['all_project_tasks'] = $all_tasks;
        $parent_data['tree'] = Task::getTaskTree();
        return $parent_data;
    }

    /**
     * Возвращает историю постранично
     * @return array
     */
    private function onGetTaskStoryData() {
        $data = post();
        $page = isset($data['page']) ? $data['page'] : 1;
        $history = HelperHistory::getDataPaginated(Task::MODEL_NAMESPACE, $data['task_id'], 10, $page);
        return ['task_id' => $data['task_id'], 'pages' => $history];
    }

    /******************************************************************************************************************/
    // сохранение полей
    /******************************************************************************************************************/

    /**
     * Праверка на доступ пользователя к проекту
     * @param $data
     * @return bool|mixed
     * @throws \ValidationException
     */
    private function defaultChecking($data) {
        HF::Validate($data, HF::DEFAULT_RULES);
        return AC::onLoadCheckAccess($data['project_slug'], AC::ACS_TSK, AC::IS_RW, $data['user_id']);
    }

    /**
     * Сохранение основных полей
     * @return array|\October\Rain\Flash\FlashBag
     * @throws \ValidationException
     */
    public function onTaskMainSave() {
        if (!$this->defaultChecking(post())) return Flash::error(AC::ERR_NO_ACCESS);

        $data = post();
        $rules = ['name' => 'required|min:1|max:191'];
        HF::Validate($data, $rules);

        $task = Task::find($data['task_id']);
        $task->name = $data['name'];
        $task->descr = $data['description'];
        $task->estimate = $data['estimate'];
        $task->extra = $data['extra'];
        if (isset($data['is_closed']) && $data['is_closed'] == "" && $task->is_closed == 0) {
            $task->is_closed = 1;
        } else if (!isset($data['is_closed']) && $task->is_closed == 1) {
            $task->is_closed = 0;
        }
        $task->save();

        return [
            Flash::success(HF::TASK_SUCCESS_UPDATE),
            Task::where('id', $data['task_id'])->select(['id', 'name', 'descr', 'estimate', 'is_closed', 'extra'])->first()
        ];

    }

    /**
     * @return array
     * @throws \ValidationException
     */
    public function onTaskStatusSave() {
        if (!$this->defaultChecking(post())) return Flash::error(AC::ERR_NO_ACCESS);

        $data = post();
        $rules = [
            'worker' => 'numeric',
            'task_state' => 'numeric',
            'task_type' => 'numeric',
            'priority' => 'numeric',
            'sprint' => 'numeric',
        ];
        HF::Validate($data, $rules);

        $task = Task::find($data['task_id']);

        // исполнитель
        $task->worker_id = ($data['worker'] > 0) ? $data['worker'] : null;

        // тип задачи
        $task->task_type_id = $data['task_type'];

        // тип приоритет
        $task->task_priority_id = $data['priority'];

        // состояние
        $task->task_state_id = $data['task_state'];

        // спринт
        if ($data['sprint'] && ($task->sprint_id != $data['sprint']) ) {
            if ($data['sprint'] > 0) {
                $task->sprint_id = $data['sprint'];
            } else {
                $task->sprint_id = null;
            }
        }

        // попытка сохранения
        try { $task->save(); } catch (Exception $e) {
            return Flash::error('Ошибка при записи');
        }

        // аватар исполнителя
        $task_avatar = Task::with('worker.avatar')->find($data['task_id']);

        $task_data = Task::with([
            'sprint' => function($q) {
                $q->select(['tazaq_lp2_sprints.id', 'tazaq_lp2_sprints.name']);
            },
            'task_type' => function($q) {
                $q->select(['tazaq_lp2_task_types.id', 'tazaq_lp2_task_types.css']);
            },
            'task_priority' => function($q) {
                $q->select(['tazaq_lp2_task_priorities.id', 'tazaq_lp2_task_priorities.css']);
            },
            'task_state' => function($q) {
                $q->select(['tazaq_lp2_task_states.id', 'tazaq_lp2_task_states.name']);
            },
        ])->where('id', $data['task_id'])
            ->first(['id', 'worker_id', 'task_type_id', 'task_priority_id', 'sprint_id', 'task_state_id']);

        $task_data['avatar'] = $task_avatar->worker->avatar->path ?? null;

        return [Flash::success(HF::TASK_SUCCESS_UPDATE), $task_data];
    }

    /**
     * Сохранение отчёта
     * @return array|\October\Rain\Flash\FlashBag
     * @throws \ValidationException
     */
    public function onTaskReportSave() {
        if (!$this->defaultChecking(post())) return Flash::error(AC::ERR_NO_ACCESS);
        $data = post();

        $task = Task::find($data['task_id']);
        $task->in_report = date('Y-m-d H:i:s');
        $task->result_text = $data['result_text'] ?: '';

        // попытка сохранения
        try { $task->save(); } catch (Exception $e) {
            return Flash::error('Ошибка при записи');
        }

        return [Flash::success(HF::TASK_SUCCESS_UPDATE)];
    }

    /**
     * Сохраняет подзадачи, возвращает количество выполненных и всего
     * @return array|\October\Rain\Flash\FlashBag
     * @throws \ValidationException
     */
    public function onTaskSubtasksSave() {
        if (!$this->defaultChecking(post())) return Flash::error(AC::ERR_NO_ACCESS);
        $data = post();

        // история
        $h = new HelperHistory(Auth::getUser()->id);

        $added_subtasks   = [];
        $changed_subtasks = [];
        $deleted_subtasks = [];

        $lpuser_id = Lpuser::getLpuserIdByUserId($data['user_id']);
        if (isset($data['subtasks']) && is_array($data['subtasks'])) {
            $subtasks = $data['subtasks'];

            foreach ($subtasks as $subtask) {
                if ($subtask['is_removed'] == 1 && $subtask['id'] == "") {
                    continue;
                } else if ($subtask['is_removed'] == 1 && $subtask['id'] != "") {
                    $subtask_model = Task::find($subtask['id']);
                    $subtask_model->deleted_at = date('Y-m-d H:i:s');
                    $subtask_model->save();

                    $deleted_subtasks[] = $subtask['name'];
                    continue;
                }


                if ($subtask['id'] !== "") {
                    $subtask_model = Task::find($subtask['id']);
                    if ($subtask_model->name != $subtask['name']) {
                        $changed_subtasks[] = $subtask['name'];
                    }
                } else {
                    $subtask_model = new Task;
                    $subtask_model->project_id = ProjectModel::getProjectIdBySlug($data['project_slug']);
                    $subtask_model->task_type_id = 2;
                    $subtask_model->task_priority_id = 1;
                    $subtask_model->task_id = $data['task_id'];
                    $subtask_model->estimate = 1.00;
                    $subtask_model->result_text = '';

                    $added_subtasks[] = $subtask['name'];
                }

                $subtask_model->worker_id = $lpuser_id;
                $subtask_model->lpuser_id = $lpuser_id;
                $subtask_model->name = $subtask['name'];

                // обновление состояния --------------------------------------------------------------------------------
                if (!isset($subtask['is_done'])) {
                    if ($subtask_model->task_state_id == 5)  {
                        $changed_subtasks[] = $subtask['name'] . '(состояние)';
                    }
                    $subtask_model->task_state_id = 1;
                }
                // если подзадача выполнена, то переносим в выполненные
                if (isset($subtask['is_done']) && $subtask['is_done'] == 'on') {
                    if ($subtask_model->task_state_id == 1)  {
                        $changed_subtasks[] = $subtask['name'] . '(состояние)';
                    }
                    $subtask_model->task_state_id = 5;
                }

                $subtask_model->save();
            }
        }

        if ($added_subtasks) $h->add('Подзадачи', implode('||', $added_subtasks));
        if ($changed_subtasks) $h->change('Подзадачи', implode('||', $changed_subtasks));
        if ($deleted_subtasks) $h->delete('Подзадачи', implode('||', $deleted_subtasks));

        // запись в историю
        if ($history = $h->save()) {
            $task = Task::find($data['task_id']);
            try { $task->history()->add( $history ); } catch (Exception $e) {}
        }

        $task_subs = Task::getSubtasksCounts($data['task_id']);
        $task_subs_data = [
            'id' => $data['task_id'],
            'sub' => $task_subs['sub'],
            'sub_done' => $task_subs['sub_done'],
        ];

        return [Flash::success(HF::TASK_SUCCESS_UPDATE), $task_subs_data ];
    }

    /**
     * Сохранение тегов
     * @return array|\October\Rain\Flash\FlashBag
     * @throws \ValidationException
     */
    public function onTaskTagsSave() {
        if (!$this->defaultChecking(post())) return Flash::error(AC::ERR_NO_ACCESS);
        $data = post();

        // история
        $h = new HelperHistory(Auth::getUser()->id);

        // нормализация массива тегов в [id, is_removed, name]
        $all_tags = [];
        $tag = [];
        foreach ($data['tags'] as $atr) {
            $tag[array_keys($atr)[0]] = array_values($atr)[0];
            if (isset($atr['name'])) {
                $all_tags[] = $tag;
                $tag = [];
            }
        }

        $task = Task::find($data['task_id']);

        $added_tags   = [];
        $deleted_tags = [];

        if (isset($data['tags']) && is_array($data['tags'])) {
            $tags = $all_tags;

            foreach ($tags as $tag) {
                $tag['name'] = mb_strtolower($tag['name']);
                // тег удален и небыло ид
                if ($tag['is_removed'] == 1 && $tag['id'] == "") {
                    continue;
                    // тег удален и был ид -> убрать связь
                } else if ($tag['is_removed'] == 1 && $tag['id'] != "") {
                    $task->tags()->detach($tag['id']);
                    $deleted_tags[] = $tag['name'];
                    continue;
                    // нет ид -> добавить связь или создать тег и добавить связь
                } else if ($tag['id'] == "") {
                    $real_tag = Tag::where('name', $tag['name'])->first(['id']);
                    if (! $real_tag) {
                        $real_tag = new Tag;
                        $real_tag->name = $tag['name'];
                        $real_tag->save();
                    }
                    $task->tags()->attach($real_tag->id);
                    $added_tags[] = $tag['name'];
                }
            }

            if ($added_tags) $h->add('Теги', implode('||', $added_tags));
            if ($deleted_tags) $h->delete('Теги', implode('||', $deleted_tags));
        }

        // запись в историю
        if ($history = $h->save()) {
            try { $task->history()->add( $history ); } catch (Exception $e) {}
        }

        return [Flash::success(HF::TASK_SUCCESS_UPDATE)];
    }

    /**
     * Сохраняет родителя задачи
     * @return array|\October\Rain\Flash\FlashBag
     * @throws \ValidationException
     */
    public function onTaskParentSave() {
        if (!$this->defaultChecking(post())) return Flash::error(AC::ERR_NO_ACCESS);
        $data = post();


        $task = Task::find($data['task_id']);
        $task->task_id = $data['parent'];

        // попытка сохранения
        try { $task->save(); } catch (Exception $e) {
            return Flash::error('Ошибка при записи');
        }

        return [Flash::success(HF::TASK_SUCCESS_UPDATE)];
    }
}