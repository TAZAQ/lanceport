<?php
namespace Tazaq\Lp2\Components;
use Tazaq\Lp2\Models\Access;
use Tazaq\Lp2\Models\Lpuser;
use Tazaq\Lp2\Models\Project;
use Auth;

class AccessChecker {
    // атрибуты "access"
    CONST ACS_PRJ = 'acs_project';
    CONST ACS_SPR = 'acs_sprint';
    CONST ACS_TSK = 'acs_task';
    CONST ACS_REP = 'acs_report';
    CONST ACS_TRC = 'acs_tracker';

    // уровни доступа
    const IS_OWNER = 777;             // владелец проекта
    const IS_CUSTOMER = 666;          // заказчик
    const IS_R = 100;                 // только чтение
    const IS_RW = 110;                // чтение+запись
    const IS_RWD = 111;               // чтение+запись+удаление


    CONST ERR_NO_ACCESS = 'Нет доступа';


    /**
     * @deprecated
     * Возвращает ID пользователя(lpuser) по реальному
     * @param $real_user_id
     * @return mixed
     */
    public static function getLpuserId($real_user_id) {
        return Lpuser::where('user_id', $real_user_id)->first(['id'])->id;
    }

    /**
     * проверка существования записи доступа пользователя и проекта
     * @param $project_id - id проекта
     * @param $real_user_id - id реального юзера
     * @param array $acs_name - массив доступа, список в константах
     * @return bool
     */
    public static function check($project_id, $real_user_id, array $acs_name) {
        $user_id = Auth::getUser()->id;
        if ($real_user_id != $user_id) return false;

        $lpuser_id = self::getLpuserId($real_user_id);

        if (! is_array($acs_name)) {
            $acs_name = [$acs_name];
        }

        $access = Access::where('project_id', $project_id)
            ->where('lpuser_id', $lpuser_id)
            ->first($acs_name);

        /* Если нет записи, то нет доступа */
        if (! $access) return false;

        $check_result = true;
        foreach ($access->attributes as $acs) {
            $check_result = $check_result && $acs;
        }

        return $check_result;
    }


    /**
     * Проверка доступа при загрузке страницы
     * @param $project_slug
     * @param string|null $property - атрибут таблицы access, к которому происходит обращение
     * @param int|null $level - уровень доступа
     * @param null $uid - user_id от пользователя
     * @return bool|mixed
     */
    public static function onLoadCheckAccess($project_slug, string $property = self::ACS_PRJ, int $level = self::IS_R, $uid=null) {
        $access_model = 'access';
        $access_att_id = 'project_id';
        $att_user_id = 'user_id';

        $project_id = Project::getProjectIdBySlug($project_slug);

        if (Auth::getUser() && ($user_id = Auth::getUser()->id)) {
            if ( $uid && ($uid != $user_id) ) return false;

            return Lpuser::with([
                $access_model => function ($q) use ($project_id, $property, $level, $access_att_id) {
                    $q->where([$access_att_id => $project_id])->where($property, '>=', $level);
                }
            ])
                ->where([$att_user_id => $user_id])->first()->access;
        } return false;
    }
}