<?php
namespace Tazaq\Lp2\Components;

use Cms\Classes\ComponentBase;
use Db;
use Illuminate\Support\Facades\Redirect;
use October\Rain\Support\Facades\Flash;
use Pheanstalk\Exception;
use Tazaq\Lp2\Classes\HelperFunctions as HF;
use Tazaq\Lp2\Classes\HelperHistory;
use Tazaq\Lp2\Components\AccessChecker as AC;
use Tazaq\Lp2\Controllers\Task_states;
use Tazaq\Lp2\Models\Lpuser;
use Tazaq\Lp2\Models\Project;
use Tazaq\Lp2\Models\Sprint as SprintModel;
use Tazaq\Lp2\Models\Task;
use Tazaq\Lp2\Models\Task_state;


class Sprint extends ComponentBase {

    /**
     * @var Collection все состояния
     */
    public $states;

    /**
     * @var Collection все состояния
     */
    public $sprint_info;


    /**
     * @return string[]
     */
    public function componentDetails()
    {
        return [
            'name' => 'Спринт',
            'description' => 'Вывод данных спринта'
        ];
    }


    /**
     * свойства компонента
     * @return array
     */
    public function defineProperties()
    {
        return [
            'id' => [
                'title' => 'Идентификатор спринта',
                'description' => 'Идентификатор спринта',
                'default' => '{{ :id }}',
                'type' => 'string',
            ],
            'slug' => [
                'title' => 'Идентификатор спринта',
                'description' => 'Идентификатор спринта',
                'default' => '{{ :slug }}',
                'type' => 'string',
            ],
        ];
    }


    /**
     * загрузка компонента
     */
    public function onRun() {
        if ($this->states = $this->loadStates()) {
            $this->sprint_info = $this->loadSprintInfo();
        } else {
            return $this->controller->run('403');
        }
    }


    /**
     * Метод загрузки состояний
     * @return bool|mixed
     */
    private function loadStates() {
        if ( $id = $this->property('id') ) {
            if ( $slug = $this->property('slug') ) {
                if (!AC::onLoadCheckAccess($slug, AC::ACS_SPR)) return false;

                $project_id = Project::where('slug', $slug)->first(['id'])->id;

                $model = Task_state::with([
                    'tasks' => function($query) use($id) {
                        $query->where('task_type_id', '<>', 2)
                            ->where('sprint_id', $id);
                        ;
                    },

                ])
                    ->where('is_visible', 1)
                    ->get(['id', 'name']);

                /* Добавление количества подзадач */
                for ($state_i = 0; $state_i < count($model); $state_i++) {
                    $tasks = $model[$state_i]['tasks'];
                    for ($task_i = 0; $task_i < count($tasks); $task_i++) {
                        $task = $tasks[$task_i];
                        $task_subs = Task::getSubtasksCounts($task->id);
                        $model[$state_i]->tasks[$task_i]['sub'] = $task_subs['sub'];
                        $model[$state_i]->tasks[$task_i]['sub_done'] = $task_subs['sub_done'];
                    }
                }

                return $model;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * Информация о спринте
     * @return mixed|null
     */
    private function loadSprintInfo() {
        if ( $id = $this->property('id') ) {
            if ( $slug = $this->property('slug') ) {
                $project_id = Project::where('slug', $slug)->first(['id'])->id;
                return SprintModel::where('id', $id)
                    ->where('project_id', $project_id)
                    ->first();
            }
            return null;
        }
        return null;
    }


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* AJAX * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
    /**
     * Перемещение задачи в другое состояние
     * @return array|\October\Rain\Flash\FlashBag
     * @throws \ValidationException
     */
    public function onTaskStateChange() {
        $data = post();

        $rules = [
            'project_id' => 'required|min:1',
            'user_id' => 'required|min:1',
            'task_id' => 'required|min:1',
            'new_state' => 'required|min:1',
        ];

        /* Валидация */
        HF::Validate($data, $rules);

        /* Проверка доступа */
        if ( $project_slug = Project::getProjectSlugById($data['project_id']) ) {
            if ( AC::onLoadCheckAccess($project_slug, AC::ACS_TSK, AC::IS_RW, $data['user_id'])) {

                $task = Task::find($data['task_id']);
                $task->task_state_id = $data['new_state'] ;

                try { $task->save(); } catch (Exception $e) {}
            } else {
                return Flash::error(AC::ERR_NO_ACCESS);
            }
        } else {
            return Flash::error(AC::ERR_NO_ACCESS);
        }
    }


    /**
     * Валидация стандартных данных спринта
     * @param $data
     * @return mixed
     * @throws \ValidationException
     */
    private function sprintDataValidation($data) {
        $rules = [
            'project_slug' => 'required|min:1',
            'user_id' => 'required|min:1',
            'sprint_id' => 'required|numeric|min:1',
        ];
        HF::Validate($data, $rules);
        return $data;
    }

    /**
     * Проверка доступа к операции
     * @param $data
     * @param $action_level
     * @return bool|mixed
     */
    private function sprintAccessData($data, $action_level) {
        return AC::onLoadCheckAccess($data['project_slug'], AC::ACS_SPR, $action_level, $data['user_id']);
    }

    /**
     * Вызов удаления спринта
     * @return array|\Illuminate\Http\RedirectResponse|\October\Rain\Flash\FlashBag
     * @throws \ValidationException
     */
    public function onSprintDelete() {
        return $this->onSprintClose(true);
    }

    /**
     * Закрытие / удаление спринта
     * @param bool $delete
     * @return array|\Illuminate\Http\RedirectResponse|\October\Rain\Flash\FlashBag
     * @throws \ValidationException
     */
    public function onSprintClose($delete=false) {
        if ( !$this->sprintAccessData( $this->sprintDataValidation( $data = post() ), AC::IS_RWD)) return Flash::error(AC::ERR_NO_ACCESS);
        if ( !$current_sprint = SprintModel::where('id', $data['sprint_id'])->first(['id', 'name']) ) return Flash::error('Такого спринта не существует');

        // проверка на наличие текущего первого не зактрытого спринта
        $today = HF::today(true);
        $after_week = HF::after_week(true);
        $has_sprint = SprintModel::with([])->where('id', '<>', $data['sprint_id'])
            ->whereRaw("('$today' BETWEEN date_start AND date_finish)")
            ->first(['id', 'name']);


        Db::beginTransaction();


        // проверка на наличие текущего первого не зактрытого спринта
        if ( !$has_sprint ) {
            $has_sprint = SprintModel::createHH($data['project_slug'], "Новый спринт [{$today} - {$after_week}]");
            if ( !$has_sprint ) { return Flash::error('Ошибка при создании нового спринта'); }
        }

        // получение задач для переноса в новый спринт
        if ($delete) {
            $not_done_tasks = Task::where('sprint_id', $data['sprint_id'])
                ->get(['id']);
        } else {
            $not_done_tasks = Task::where('sprint_id', $data['sprint_id'])
                ->where('task_state_id', '<>', 5)
                ->get(['id']);
        }

        // если есть задачи для переноса
        if ($not_done_tasks) {

            // перенос
            $ok = true;
            foreach ($not_done_tasks as $t) {
                $ok = $ok && Task::onTaskSprintMoveHH($t->id, $data['user_id'], ['id' => $has_sprint->id, 'name' => $has_sprint->name]);
                if (!$ok) {
                    Db::rollBack();
                    break;
                }
            }

            // удаление / закрытие - запись в историю
            if ($delete) {
                try {
                    $current_sprint->destroy($current_sprint->id);
                } catch (Exception $e) {Db::rollBack();}
            } else {
                try {
                    $current_sprint->date_finish = HF::today(true);
                    $current_sprint->save();
                } catch (Exception $e) {Db::rollBack();}
            }


            Db::commit();
        }

        return Redirect::to("/projects/{$data['project_slug']}/sprints/{$has_sprint->id}");
    }

    /**
     * Сохранение спринта
     * @return array|\October\Rain\Flash\FlashBag
     * @throws \ValidationException
     */
    public function onSprintSave() {
        if ( !$this->sprintAccessData( $this->sprintDataValidation( $data = post() ), AC::IS_RW)) return Flash::error(AC::ERR_NO_ACCESS);

        $fields = ['name', 'descr', 'date_start', 'date_finish'];

        $sprint = SprintModel::find($data['sprint_id']);
        foreach ($fields as $field) if ($data[$field]) $sprint->$field = $data[$field];
        $sprint->project_id = Project::getProjectIdBySlug($data['project_slug']);

        $sprint->save();

        return [Flash::success('Спринт успешно обновлён'), ['name' => $sprint->name]];
    }
}