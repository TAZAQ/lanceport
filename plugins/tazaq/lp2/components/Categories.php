<?php
namespace Tazaq\Lp2\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Input;
use Exception;
use Tazaq\Lp2\Classes\HelperHistory;
use Tazaq\Lp2\Models\Category;
use Tazaq\Lp2\Models\Lpuser;
use Tazaq\Lp2\Models\Project;
use Flash;
use Tazaq\Lp2\Classes\HelperFunctions as HF;
use System\Models\File as FileModel;
use Auth;
use Str;



class Categories extends ComponentBase
{

    /**
     * @var Collection категории для отображения
     */
    public $categories;


    /**
     * Описание компонента
     */
    public function componentDetails()
    {
        return [
            'name' => 'Список категорий',
            'description' => 'Вывод списка категорий'
        ];
    }


    /**
     *  Поля компонента
     */
    public function defineProperties()
    {
        return [
            'slug' => [
                'title' => 'Url',
                'description' => 'Url адрес категории',
                'default' => '{{ :slug }}',
                'type' => 'string',
            ],
        ];
    }

    /**
     * записывает в 'categories' все категории
     */
    public function onRun() {
        $this->categories = $this->loadCategories();
    }


    /**
     * @return mixed вернет все категории
     */
    protected function loadCategories() {
        $id = Auth::getUser()->id;

        // загрузка проектов, к которым есть доступ
        $user_projects = Lpuser::with(['projects.project_image', 'projects.category'])
            ->where('user_id', $id)
            ->first()
            ->projects;

        $new_categories = [];
        foreach ($user_projects as $project) {
            $new_categories["{$project->category->id}"][] = $project;
        }

        $categories = Category::get();
        foreach ($categories as &$category) {
            $projects = isset($new_categories["{$category->id}"]) ? $new_categories["{$category->id}"] : [];
            $category->attributes['projects'] = $projects;
        }

        return $categories;
    }


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* AJAX * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    /**
     * Метод для добавления и изменения категории
     * @param bool $is_update
     * @return array|null
     */
    public function onCategoryNew($is_update = false) {
        $data = post();

        $rules = [
            'category_name' => 'required|min:5|max:50',
            'category_description' => 'max:100'
        ];

        /* Валидация */
        HF::Validate($data, $rules);


        $name = post('category_name');
        $description = post('category_description');

        $cur_date = date('Y-m-d H:i:s');
        $slug = HF::slugRuCreate($name);

        /* TODO как достать юзера?*/


        if ($is_update) {
            $id = post('id');
            $result = Category::where('id', $id)->update(
                [
                    'name' => $name,
                    'descr' => $description,
                    'slug' => $slug,
                ]
            );

            if ($result) {
                return [
                    'is_update' => true,
                    'data' => ['id' => $id, 'name' => $name, 'description' => $description, 'slug' => $slug]
                ];
            }
        } else {
            $result = Category::insertGetId(
                [
                    'name' => $name,
                    'descr' => $description,
                    'slug' => $slug,
                    'created_at' => $cur_date,
                    'updated_at' => $cur_date
                ]
            );
        }

        if ($result) {
            Flash::success('Готово!');

            return [
                'data' => Category::where('id', $result)->first(['id', 'name', 'descr', 'slug'])
            ];
        } else {
            Flash::error('Ошибка при создании категории');
            return null;
        }
    }


    /**
     * Метод отдающий поля по id
     * @return array
     */
    public function onCategoryLoad() {
        $id = post('id');

        return [
            'data' => Category::where('id', $id)->first(['name', 'descr'])
        ];
    }


    /**
     * Метод, возвращающий список категорий
     * @return array
     */
    public function onCategoryListLoad() {
        return [
            'data' => Category::orderBy('name')->get(['id', 'name'])
        ];
    }


    /**
     * метод изменения категории
     * @return mixed|null
     */
    public function onCategorySave() {
        $result = $this->onCategoryNew(true);

        if ($result['is_update']) {
            Flash::success('Готово!');

            return $result['data'];
        } else {
            Flash::error('Ошибка при изменении категории');
            return null;
        }
    }

    /**
     * метод создания проекта
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|Project
     * @throws \ImagickException
     */
    public function onProjectNew() {
        // получение данных и валидация
        $data = post();

        $rules = [
            'category' => 'required|min:1',
            'name' => 'required|min:5|max:100',
            'description' => 'max:1000',
            'date_start' => 'required|date',
            'date_finish' => 'required|date',
        ];

        /* Валидация */
        HF::Validate($data, $rules);


        // переменные для бд
        $category_id = post('category');
        $name = post('name');
        $description = post('description');
        $date_start = post('date_start');
        $date_finish = post('date_finish');

        $cur_date = date('Y-m-d H:i:s');
        $slug = HF::slugRuCreate($name);

        /* TODO как достать юзера?*/

        // проверка на существование записи
        $is_exists = Project::where('slug', $slug)->first(['id']);
        if ($is_exists) {
            Flash::error('Такой проект уже существует!');
            return null;
        } else {
            $project = new Project();
            $project->category_id = $category_id;
            $project->name = $name;
            $project->slug = Str::slug($name);
            $project->descr = $description;
            $project->planned_date_start = $date_start;
            $project->planned_date_finish = $date_finish;
            $project->save();

            if (Input::hasFile('project_image')) {
                HF::resizeImage(Input::file('project_image')->getRealPath());

                $file = new FileModel;
                $file->data = Input::file('project_image');
                $file->is_public = true;
                $file->save();

                $project->project_image()->add($file);

                // завпись в историю
                $h = new HelperHistory(Auth::getUser()->id);
                $h->update('Изображение', $project->project_image->path);

                // запись в историю
                if ($history = $h->save()) {
                    try { $project->history()->add( $history ); } catch (Exception $e) {}
                }
            }

            if ($project) {
                Flash::success("Проект {$name} добавлен");
                return Project::with('project_image')
                    ->where('id', $project->id)
                    ->first(['category_id', 'name', 'descr', 'slug']);
            } else {
                Flash::error('Ошибка при создании проекта');
                return null;
            }
        }
    }
}
