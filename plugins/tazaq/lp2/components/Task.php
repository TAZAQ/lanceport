<?php
namespace Tazaq\Lp2\Components;

use Cms\Classes\ComponentBase;
use Tazaq\Lp2\Components\AccessChecker as AC;
use Tazaq\Lp2\Models\Comment;
use Tazaq\Lp2\Models\Lpuser;
use Tazaq\Lp2\Models\Project;
use Tazaq\Lp2\Models\Task as TaskModel;

class Task extends ComponentBase {

    public $task;


    /**
     * @return string[]
     */
    public function componentDetails()
    {
        return [
            'name' => 'Задача с комментариями',
            'description' => 'Вывод задач в ленту'
        ];
    }


    /**
     * свойства компонента
     * @return array
     */
    public function defineProperties()
    {
        return [
            'id' => [
                'title' => 'Идентификатор задачи',
                'description' => 'Идентификатор задачи',
                'default' => '{{ :id }}',
                'type' => 'string',
            ],
            'slug' => [
                'title' => 'Идентификатор проекта',
                'description' => 'Идентификатор проекта',
                'default' => '{{ :slug }}',
                'type' => 'string',
            ],
        ];
    }


    /**
     * загрузка компонента
     */
    public function onRun() {
        if (!$this->task = $this->loadTaskWithComments()) {
            return $this->controller->run('403');
        }
    }


    /**
     * загрузка комментариев
     * @return mixed|null
     */
    private function loadTaskWithComments() {
        if ( $id = $this->property('id') ) {
            if ( $slug = $this->property('slug') ) {
                if (!AC::onLoadCheckAccess($slug)) return false;


                $project_id = Project::where('slug', $slug)->first(['id'])->id;

                $model = TaskModel::with([
                    'tags',
                    'sprint',
                    'task_priority',
                    'task_type',
                    'lpuser'
                ])
                    ->where('project_id', $project_id)
                    ->where('id', $id)
                    ->first();

                $lpuser_type_id = Lpuser::where('user_id', \Auth::getUser()->id)->first(['user_type_id'])->user_type_id ?? null;

                if (!$model || !$model->in_report && ($lpuser_type_id == 2)) return false;

                $tree_comments = Comment::with(['parent', 'children'])
                    ->where('task_id', $id)
                    ->orderBy('created_at')
                    ->get()
                    ->toNested();

                $model['comments'] = $tree_comments;

                return $model;
            }
        }

        return null;
    }


    /**
     * Добавляет комментарий к задаче
     * @return array|\October\Rain\Flash\FlashBag|Comment
     */
    public function onSendMessage() {

        if (($project_slug = $this->property('slug')) &&
            ($task_id = $this->property('id')) &&
            ($uid = \Auth::getUser()->id)) {

            if (! AC::onLoadCheckAccess($project_slug, AC::ACS_REP, AC::IS_RW, $uid)) {
                return \Flash::error(AC::ERR_NO_ACCESS);
            }
        } else return \Flash::error(AC::ERR_NO_ACCESS);

        $data = post();
        $text = $data['comment_text'];
        $url = '@^(https?|ftp)://[^\s/$.?#].[^\s]*$@iS';
        $text = preg_replace($url, '<a href="http$2://$4" target="_blank" title="$0">$0</a>', $text);
        $text = preg_replace('#(<script*>.*</script>)#sU', '', $text);

        $comment = new Comment();
        $comment->task_id = $task_id;
        $comment->lpuser_id = Lpuser::getLpuserIdByUserId($uid);
        if (isset($data['comment_id'])) {
            $comment->comment_id = $data['comment_id'];
            $comment->is_read = Comment::find($data['comment_id'], ['lpuser_id'])->lpuser_id;
        }
        $comment->text = $text;
        $comment->save();

        return ["link" => "/projects/{$project_slug}/tasks/{$task_id}", 'hash' => $comment->id];
    }
}