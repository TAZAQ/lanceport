<?php namespace Tazaq\Lp2\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateTazaqLp2UsersProjectsAccess extends Migration
{
    public function up()
    {
        Schema::create('tazaq_lp2_users_projects_access', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('lpuser_id')->unsigned();
            $table->integer('project_id')->unsigned();
            $table->integer('acs_project')->default(0);
            $table->integer('acs_sprint')->default(0);
            $table->integer('acs_task')->default(0);
            $table->integer('acs_report')->default(0);
            $table->integer('acs_tracker')->default(0);
            
            $table->foreign('lpuser_id')->references('id')->on('tazaq_lp2_lpusers');
            $table->foreign('project_id')->references('id')->on('tazaq_lp2_projects');
    
    
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('tazaq_lp2_users_projects_access');
    }
}
