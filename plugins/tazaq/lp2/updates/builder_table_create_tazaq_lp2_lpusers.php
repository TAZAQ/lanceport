<?php namespace Tazaq\Lp2\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateTazaqLp2Lpusers extends Migration
{
    public function up()
    {
        Schema::create('tazaq_lp2_lpusers', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->integer('user_id')->unsigned();
            $table->integer('user_type_id')->unsigned();
            $table->string('name');
            $table->string('initials', 3)->nullable();
            $table->text('about')->nullable();
            $table->string('social')->nullable();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('user_type_id')->references('id')->on('tazaq_lp2_user_types');
    
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('tazaq_lp2_lpusers');
    }
}
