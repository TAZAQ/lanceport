<?php namespace Tazaq\Lp2\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateTazaqLp2TaskStates extends Migration
{
    public function up()
    {
        Schema::create('tazaq_lp2_task_states', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->string('name');
            $table->string('descr');
            $table->boolean('is_visible')->default(1);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('tazaq_lp2_task_states');
    }
}
