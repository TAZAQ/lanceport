<?php namespace Tazaq\Lp2\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateTazaqLp2Sprints extends Migration
{
    public function up()
    {
        Schema::create('tazaq_lp2_sprints', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->integer('project_id')->unsigned();
            $table->string('name');
            $table->text('descr');
            $table->dateTime('date_start');
            $table->dateTime('date_finish');

            $table->foreign('project_id')->references('id')->on('tazaq_lp2_projects');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('tazaq_lp2_sprints');
    }
}