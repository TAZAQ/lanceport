<?php namespace Tazaq\Lp2\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateTazaqLp2TasksLpusersComments extends Migration
{
    public function up()
    {
        Schema::create('tazaq_lp2_tasks_lpusers_comments', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->integer('task_id')->unsigned();
            $table->integer('lpuser_id')->unsigned();
            $table->integer('comment_id')->unsigned()->nullable();
            $table->text('text');
            $table->integer('is_read')->default(0);
            
            $table->foreign('task_id')->references('id')->on('tazaq_lp2_tasks');
            $table->foreign('lpuser_id')->references('id')->on('tazaq_lp2_lpusers');
            $table->foreign('comment_id')->references('id')->on('tazaq_lp2_tasks_lpusers_comments');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('tazaq_lp2_tasks_lpusers_comments');
    }
}