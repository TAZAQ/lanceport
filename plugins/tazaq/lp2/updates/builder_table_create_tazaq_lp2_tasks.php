<?php namespace Tazaq\Lp2\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateTazaqLp2Tasks extends Migration
{
    public function up()
    {
        Schema::create('tazaq_lp2_tasks', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->integer('project_id')->unsigned();
            $table->integer('task_type_id')->unsigned();
            $table->integer('task_priority_id')->unsigned();
            $table->integer('task_id')->unsigned()->nullable();
            $table->integer('lpuser_id')->unsigned();
            $table->integer('worker_id')->unsigned();
            $table->integer('sprint_id')->unsigned()->nullable();
            $table->integer('task_state_id')->unsigned();
            $table->string('name');
            $table->text('descr')->nullable();
            $table->decimal('estimate', 10, 2)->default(1.00);
            $table->text('result_text')->nullable();
            $table->timestamp('in_report')->nullable()->default(null);
            $table->boolean('is_tracked')->default(0);
            $table->boolean('is_closed')->default(0);
            $table->string('extra')->nullable();
            
            $table->foreign('project_id')->references('id')->on('tazaq_lp2_projects');
            $table->foreign('task_type_id')->references('id')->on('tazaq_lp2_task_types');
            $table->foreign('task_priority_id')->references('id')->on('tazaq_lp2_task_priorities');
            $table->foreign('task_id')->references('id')->on('tazaq_lp2_tasks');
            $table->foreign('lpuser_id')->references('id')->on('tazaq_lp2_lpusers');
            $table->foreign('worker_id')->references('id')->on('tazaq_lp2_lpusers');
            $table->foreign('sprint_id')->references('id')->on('tazaq_lp2_sprints');
            $table->foreign('task_state_id')->references('id')->on('tazaq_lp2_task_states');

        });
    }
    
    public function down()
    {
        Schema::dropIfExists('tazaq_lp2_tasks');
    }
}
