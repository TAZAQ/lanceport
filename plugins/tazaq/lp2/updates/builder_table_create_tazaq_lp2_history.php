<?php namespace Tazaq\Lp2\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateTazaqLp2History extends Migration
{
    public function up()
    {
        Schema::create('tazaq_lp2_history', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->integer('lpuser_id')->unsigned();
            $table->string('lp2model_type');
            $table->integer('lp2model_id');
            $table->text('data');

            $table->foreign('lpuser_id')->references('id')->on('tazaq_lp2_lpusers');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('tazaq_lp2_history');
    }
}