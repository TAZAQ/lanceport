<?php namespace Tazaq\Lp2\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateTazaqLp2Categories extends Migration
{
    public function up()
    {
        Schema::create('tazaq_lp2_categories', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->string('name', 100);
            $table->text('descr');
            $table->string('slug', 191)->index();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('tazaq_lp2_categories');
    }
}