<?php namespace Tazaq\Lp2\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateTazaqLp2TasksTags extends Migration
{
    public function up()
    {
        Schema::create('tazaq_lp2_tasks_tags', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('task_id')->unsigned();
            $table->integer('tag_id')->unsigned();
            $table->primary(['task_id','tag_id']);
            
            $table->foreign('task_id')->references('id')->on('tazaq_lp2_tasks');
            $table->foreign('tag_id')->references('id')->on('tazaq_lp2_tags');
    
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('tazaq_lp2_tasks_tags');
    }
}
