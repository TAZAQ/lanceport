<?php namespace Tazaq\Lp2\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateTazaqLp2Projects extends Migration
{
    public function up()
    {
        Schema::create('tazaq_lp2_projects', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->integer('category_id')->unsigned();
            $table->string('name', 100);
            $table->text('descr');
            $table->string('slug')->index();
            $table->dateTime('planned_date_start')->nullable();
            $table->dateTime('planned_date_finish')->nullable();
            $table->dateTime('real_date_finish')->nullable();
            $table->string('extra')->nullable();

            $table->foreign('category_id')->references('id')->on('tazaq_lp2_categories');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('tazaq_lp2_projects');
    }
}