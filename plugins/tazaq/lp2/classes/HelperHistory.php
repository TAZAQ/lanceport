<?php

namespace Tazaq\Lp2\Classes;


use Tazaq\Lp2\Models\Lpuser;
use Tazaq\Lp2\Models\History;

class HelperHistory {

    const ACTION_CREATE = 'Created';
    const ACTION_UPDATE = 'Updated';
    const ACTION_DELETE = 'Deleted';
    const ACTION_MOVE   = 'Moved';
    const ACTION_CHANGE = 'Changed';
    const ACTION_ADD    = 'Added';
    const ACTION_SET    = 'Set';
    const ACTION_CLOSE  = 'Closed';


    private $history_array;
    private $user_id;

    /**
     * HelperHistory constructor.
     * @param $user_id
     */
    public function __construct($user_id) {
        $this->history_array = [
            self::ACTION_CREATE => [],
            self::ACTION_ADD    => [],
            self::ACTION_SET    => [],
            self::ACTION_UPDATE => [],
            self::ACTION_MOVE   => [],
            self::ACTION_CHANGE => [],
            self::ACTION_DELETE => [],
            self::ACTION_CLOSE  => [],
        ];

        $this->user_id = $user_id;
    }


    //******************************************************************************************************************
    //----статика----статика----статика----статика----статика----статика----статика----статика----статика----статика----
    //******************************************************************************************************************

    /**
     * Возвращает количество записей по задаче
     * @param $model_type
     * @param int $model_id
     * @return int
     */
    public static function getRecordsCount($model_type, int $model_id): int {
        return History::where('lp2model_id', $model_id)
            ->where('lp2model_type', $model_type)
            ->count();
    }

    /**
     * Вернёт данные постранично
     *
     * @param $model_type
     * @param int $model_id
     * @param int $records_on_page
     * @param int $page
     * @return mixed
     */
    public static function getDataPaginated($model_type, int $model_id, $records_on_page=25, $page=1) {
        return History::with([
            'lpuser' => function ($q) {
                $q->select(['id']);
            },
            'lpuser.avatar'
        ])
            ->where('lp2model_id', $model_id)
            ->select(['data', 'created_at', 'lpuser_id'])
            ->where('lp2model_type', $model_type)
            ->orderByDesc('created_at')
            ->paginate($records_on_page, $page);
    }


    //******************************************************************************************************************
    //-------приватные----приватные----приватные----приватные----приватные----приватные----приватные----приватные-------
    //******************************************************************************************************************

    /**
     * Записывает данные в массив истории
     * @param string $action
     * @param string $attribute
     * @param string $value
     */
    private function writeToHistory(string $action, string $attribute, string $value) {
        $this->history_array[$action][] = ['label' => $attribute, 'value' => $value];
    }


    //******************************************************************************************************************
    //-------публичные----публичные----публичные----публичные----публичные----публичные----публичные----публичные-------
    //******************************************************************************************************************

    /**
     * Вернёт массив истории
     * @return array[]
     */
    public function getHistoryArray(): array {
        return $this->history_array;
    }

    /**
     * действие: создание
     * @param string $attribute - поле
     * @param string $value - новое значение
     */
    public function create(string $attribute, string $value) {
        $this->writeToHistory(self::ACTION_CREATE, $attribute, $value);
    }

    /**
     * действие: обновление
     * @param string $attribute - поле
     * @param string $value - новое значение
     */
    public function update(string $attribute, string $value) {
        $this->writeToHistory(self::ACTION_UPDATE, $attribute, $value);
    }

    /**
     * действие: удаление
     * @param string $attribute - поле
     * @param string $value - новое значение
     */
    public function delete(string $attribute, string $value) {
        $this->writeToHistory(self::ACTION_DELETE, $attribute, $value);
    }

    /**
     * действие: перемещение
     * @param string $attribute - поле
     * @param string $value - новое значение
     */
    public function move(string $attribute, string $value) {
        $this->writeToHistory(self::ACTION_MOVE, $attribute, $value);
    }

    /**
     * действие: изменение
     * @param string $attribute - поле
     * @param string $value - новое значение
     */
    public function change(string $attribute, string $value) {
        $this->writeToHistory(self::ACTION_CHANGE, $attribute, $value);
    }

    /**
     * действие: добавление
     * @param string $attribute - поле
     * @param string $value - новое значение
     */
    public function add(string $attribute, string $value) {
        $this->writeToHistory(self::ACTION_ADD, $attribute, $value);
    }

    /**
     * действие: завершение
     * @param string $attribute - поле
     * @param string $value - новое значение
     */
    public function close(string $attribute, string $value) {
        $this->writeToHistory(self::ACTION_CLOSE, $attribute, $value);
    }

    /**
     * действие: установка
     * @param string $attribute - поле
     * @param string $value - новое значение
     */
    public function set(string $attribute, string $value) {
        $this->writeToHistory(self::ACTION_SET, $attribute, $value);
    }


    /**
     * Запись изменений в базу
     * @return null если нет изменений
     */
    public function save() {
        $cnt = 0;
        $simple_history_array = [];
        foreach ($this->history_array as $key => $val) {
            $cnt += count($val);
            if (count($val) > 0) $simple_history_array[$key] = $val;
        }
        if ($cnt == 0) return;

        $history_record = new History();
        $history_record->lpuser_id = Lpuser::getLpuserIdByUserId($this->user_id);
        $history_record->data = json_encode($simple_history_array);
        $history_record->save();

        return $history_record;
    }
}