<?php
namespace Tazaq\Lp2\Classes;

use Validator;
use ValidationException;
use Imagick;

class HelperFunctions {
    const ACCESS_RULES = [
        'project_slug' => 'required|min:1',
        'user_id' => 'required|min:1',
    ];

    const DEFAULT_RULES = [
        'project_slug' => 'required|min:1',
        'user_id' => 'required|min:1',
        'task_id' => 'required|min:1',
    ];

    const TASK_SUCCESS_UPDATE = 'Задача обновлена';

    public static function pprint($data) {
        echo "<pre>";
        var_dump($data);
        echo "</pre>";
    }

    /**
     * @param $ru_text "русский текст"
     * @return string "russkiy tekst"
     */
    public static function slugRuCreate($ru_text) {
        $en_alphabet = 'abcdefghijklmnopqrstuvwxyz';
        $ru_to_en_table = [
            'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g',
            'д' => 'd', 'е' => 'e', 'ё' => 'e', 'ж' => 'zh',
            'з' => 'z', 'и' => 'i', 'й' => 'y', 'к' => 'k',
            'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o',
            'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't',
            'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c',
            'ч' => 'ch', 'ш' => 'sh', 'щ' => 'shch', 'ь' => '',
            'ъ' => '', 'ы' => 'i', 'э' => 'e', 'ю' => 'yu',
            'я' => 'ya', "\s" => "-", ' ' => '-', '-' => '-'
        ];

        $text = mb_strtolower($ru_text);
        $text = preg_replace("/[^а-яёa-z0-9\-\s]/iu", '', $text);

        $slug = "";
        for ($i = 0; $i < mb_strlen($text); $i++) {
            $symbol = mb_substr($text, $i, 1);

            if (is_numeric($symbol) || stristr($en_alphabet, $symbol)) $new_symbol = $symbol;
            else $new_symbol = $ru_to_en_table[$symbol];

            $new_symbol = is_null($new_symbol) ? '-' : $new_symbol;
            $slug = $slug . $new_symbol;
        }

        return $slug;
    }

    /**
     * Валидатор $data по $rules
     * @param $data
     * @param $rules
     * @throws ValidationException
     */
    public static function Validate($data, $rules) {
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            throw new ValidationException($validator);
        }
    }

    /**
     * Изменятель размера изображения
     * @param $path
     * @throws \ImagickException
     */
    public static function resizeImage($path) {
        $image_size = 75;
        $thumb = new Imagick($path);
        $thumb->scaleImage($image_size, $image_size, false);
        $thumb->writeImage($path);
        $thumb->destroy();
    }

    /**
     * Вернёт текущее дату/врямя в формате YYYY-mm-dd HH:ii:ss
     * @param bool $time - если true со временем
     * @return string
     */
    public static function today($time = false) {
        if (!$time) return date('Y-m-d');
        else return date('Y-m-d H:i:s');
    }

    /**
     * Вернёт дату/врямя через неделю в формате YYYY-mm-dd HH:ii:ss
     * @param bool $time
     * @return false|string
     */
    public static function after_week($time = false) {
        if (!$time) return date("Y-m-d", strtotime("+7 days"));
        else return date("Y-m-d H:i:s", strtotime("+7 days"));
    }

    /**
     * проверка строки на дату
     * @param $str
     * @return bool
     */
    public static function is_Date($str){
        return is_numeric(strtotime($str));
    }
}